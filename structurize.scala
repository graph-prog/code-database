

// export SERIALIZATION_PROTOCOL=1; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | scala -savecompiled -nc -J-Xms8g -J-Xmx8g -J-Xss1g structurize.scala | hd


import scala.collection.mutable.{ListBuffer => List}
import scala.collection.mutable.{LinkedHashMap => Map}
import scala.collection.mutable.Stack


object state {
	var symbol_tables = List[Map[String, Any]]()
	var symtab_nodes:List[Map[String, Any]] = null
	var tree_node_stack:Stack[Map[String, Any]] = null
	var current:Map[String, Any] = null
}

var refs = Map[String, Map[String, Any]]()


io.Source.stdin.getLines()
.map(line => line.split(" +").toList)
.foreach(line => {
	
	
	line(0) match {
		
		case "A" => {
			// pop/push symbol_table
			
			state.symbol_tables += Map()
			state.current = state.symbol_tables.last
			state.symtab_nodes = null
			state.tree_node_stack = Stack(state.current)
			
		}
		case "B" => {
			// pop/push symtab_node
			
			if(state.symtab_nodes == null) { // append to end
				state.symtab_nodes = List[Map[String, Any]]()
				state.current("symbol_table.$nodes") = state.symtab_nodes
			}
			
			state.symtab_nodes += Map()
			state.current = state.symtab_nodes.last
			state.tree_node_stack = Stack(state.current)
			
		}
		case "C" => {
			// push tree_node, step 2
			//
			// printf("C SELECTOR %d %s  LEN %d  TCC %d %s  TNL %d %s  (%016llx)\n", SELECTOR, _SELECTOR, LEN, TCC, _TCC, TNL, _TNL, t)
			
			state.current = state.tree_node_stack.top
			
			state.current(".selector") = (line(3), "%s")
			state.current(".len") = (line(5), "%d")
			state.current(".tcc") = (line(8), "%s")
			state.current(".tnl") = (line(11), "%s")
			state.current(".addr") = (line(12).substring(1, line(12).length - 1), "%x")
			
			refs(state.current(".addr").asInstanceOf[(String, String)]._1) = state.current
			
		}
		case "D" => {
			// push tree_node, step 2 (reference to other tree_node)
			//
			// printf("D REF  (%016llx)\n", t)
			
			//state.current.addAll(refs(line(2).substring(1, line(2).length - 1)))
			//for(x <- refs(line(2).substring(1, line(2).length - 1))) state.current += x
			state.current(".ref") = line(2).substring(1, line(2).length - 1) // TODO activate when ready
			
		}
		case "E" => {
			// push tree_node, step 1 (node in field)
			//
			// 3 - contains node in field
			// HANDLE_TREENODELAYOUT3(TYPE, FIELD)
			// printf("E %s.%s {\n", #TYPE, #FIELD)
			
			if(line(1) contains "[") {
				var list = List[Map[String, Any]]()
				if(state.current isDefinedAt line(1))
					list = state.current(line(1)).asInstanceOf[List[Map[String, Any]]]
				else
					state.current(line(1)) = list
				state.current = Map()
				list += state.current
			}
			else {
				state.current(line(1)) = Map()
				state.current = state.current(line(1)).asInstanceOf[Map[String, Any]]
			}
			
			state.tree_node_stack.push(state.current)
			
		}
		case "F" => {
			// pop tree_node (node in field)
			
			state.tree_node_stack.pop
			state.current = state.tree_node_stack.top
			
		}
		case "G" => {
			// push tree_node, step 1 (node not in field)
			//
			// 4 - contains node elsewhere but not in field
			// HANDLE_TREENODELAYOUT4(NAME, VALUE)
			// printf("G %s {\n", #NAME)
			
			if(line(1) contains "[") {
				var list = List[Map[String, Any]]()
				if(state.current isDefinedAt line(1))
					list = state.current(line(1)).asInstanceOf[List[Map[String, Any]]]
				else
					state.current(line(1)) = list
				state.current = Map()
				list += state.current
			}
			else {
				state.current(line(1)) = Map()
				state.current = state.current(line(1)).asInstanceOf[Map[String, Any]]
			}
			
			state.tree_node_stack.push(state.current)
			
		}
		case "H" => {
			// pop tree_node (node not in field)
			
			state.tree_node_stack.pop
			state.current = state.tree_node_stack.top
			
		}
		case "I" => {
			// set property (was in field)
			//
			// 5 - contains property directly in field
			// HANDLE_TREENODELAYOUT5(TYPE, FORMAT, FIELD)
			// printf("I %s.%s [%s]: " FORMAT "\n", #TYPE, #FIELD, FORMAT, TNL_FIELD(TYPE, FIELD))
			
			if(line(1) contains "[") {
				var list = List[String]()
				if(state.current isDefinedAt line(1))
					list = state.current(line(1)).asInstanceOf[(List[String], String)]._1
				else
					state.current(line(1)) = (list, line(2).substring(1, line(2).length - 2))
				list += line(3)
			}
			else {
				state.current(line(1)) = (line(3), line(2).substring(1, line(2).length - 2))
			}
			
		}
		case "J" => {
			// set property (was not in field)
			//
			// 6 - contains property indirectly or not in field
			// HANDLE_TREENODELAYOUT6(NAME, FORMAT, VALUE)
			// printf("J %s [%s]: " FORMAT "\n", #NAME, FORMAT, VALUE)
			//
			// 7 - contains property at offset
			// HANDLE_TREENODELAYOUT7(TYPE, FORMAT, SIZE, OFFSET, FIELD)
			// HANDLE_TREENODELAYOUT6(TYPE.FIELD, FORMAT, FIELD)
			
			if(line(1) contains "[") {
				var list = List[String]()
				if(state.current isDefinedAt line(1))
					list = state.current(line(1)).asInstanceOf[(List[String], String)]._1
				else
					state.current(line(1)) = (list, line(2).substring(1, line(2).length - 2))
				list += line(3)
			}
			else {
				state.current(line(1)) = (line(3), line(2).substring(1, line(2).length - 2))
			}
			
		}
		case "X" => {
			// pass
			
		}
		case "Y" => {
			// pass
			
		}
		case "Z" => {
			// unhandled treecode
			
			Console.err.println("unhandled treecode: ", line)
			
		}
		case _ => {
			// unhandled record type
			
			Console.err.println("unhandled record type: ", line)
			
		}
	}
})


new java.io.ObjectOutputStream(System.out).writeObject((state.symbol_tables, refs))


Console.err.println("structurize exit")

