

# export SERIALIZATION_PROTOCOL=1; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | python3 structurize.py | python3 pretty2.py


import os
import pickle
import sys
from lib.serialize import *


while True:
	symbol_table = serialize_read()
	if not symbol_table:
		break
	
	serialize_pretty(symbol_table)


