

// 0.1: unpack gcc-12.2.0 to ~
// 0.2: run ~/gcc-12.2.0$ /contrib/download_prerequisites
// 0.3: run ~/gcc-12.2.0$ ./configure --disable-multilib
// 0.4: create ~/demo.c
// 0.5: apply patch

// 1: ~/gcc-12.2.0$ make -j 64
// 2: ~$ STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1


#include "tree.h"
#include "cgraph.h"
#include "tree-dump.h"
#include <string>
#include <unordered_set>


FILE *store_graph_to_disk__fd;
std::unordered_set<void*> store_graph_to_disk__ref;


// TODO rewrite and diff


enum store_graph_to_disk__tree_node_layout {
	
	tnl_base,
	tnl_typed,
	tnl_common,
	tnl_int_cst,
	tnl_poly_int_cst,
	tnl_real_cst,
	tnl_fixed_cst,
	tnl_vector,
	tnl_string,
	tnl_complex,
	tnl_identifier,
	tnl_decl_minimal,
	tnl_decl_common,
	tnl_decl_with_rtl,
	tnl_decl_non_common,
	tnl_parm_decl,
	tnl_decl_with_vis,
	tnl_var_decl,
	tnl_field_decl,
	tnl_label_decl,
	tnl_result_decl,
	tnl_const_decl,
	tnl_type_decl,
	tnl_function_decl,
	tnl_translation_unit_decl,
	tnl_type_common,
	tnl_type_with_lang_specific,
	tnl_type_non_common,
	tnl_list,
	tnl_vec,
	tnl_exp,
	tnl_ssa_name,
	tnl_block,
	tnl_binfo,
	tnl_statement_list,
	tnl_constructor,
	tnl_omp_clause,
	tnl_optimization_option,
	tnl_target_option,
};


#ifndef TNL_HANDLER_SIGNATURE
#define TNL_HANDLER_SIGNATURE(SELECTOR) \
	\
	static void store_graph_to_disk__tree_node2__##SELECTOR(unsigned int PASS, unsigned int LEN, tree_node* t)
	
#endif


#ifndef TNL_FIELD
#define TNL_FIELD(TYPE, FIELD) ((struct TYPE*) t)->FIELD
#endif


// 1 - initial call
#ifndef HANDLE_TREENODELAYOUT
#define HANDLE_TREENODELAYOUT(PASS, SELECTOR) \
	\
	if(TNL == SELECTOR) \
		store_graph_to_disk__tree_node2__##SELECTOR(PASS, LEN, t);
	
#endif


// 2 - extends node
#ifndef HANDLE_TREENODELAYOUT2
#define HANDLE_TREENODELAYOUT2(SELECTOR) \
	\
	store_graph_to_disk__tree_node2__##SELECTOR(PASS, LEN, t);
	
#endif


// 3 - contains node in field
#ifndef HANDLE_TREENODELAYOUT3
#define HANDLE_TREENODELAYOUT3(TYPE, FIELD) \
	\
	if(PASS == 2) { \
		fprintf(store_graph_to_disk__fd, "E %s.%s {\n", #TYPE, "$" #FIELD); \
		store_graph_to_disk__tree_node((tree_node*) TNL_FIELD(TYPE, FIELD)); \
		fprintf(store_graph_to_disk__fd, "F %s.%s }\n", #TYPE, "$" #FIELD); \
	}
	
#endif


// 4 - contains node elsewhere but not in field
#ifndef HANDLE_TREENODELAYOUT4
#define HANDLE_TREENODELAYOUT4(TYPE, FIELD, VALUE) \
	\
	if(PASS == 2) { \
		fprintf(store_graph_to_disk__fd, "G %s.%s {\n", #TYPE, "$" #FIELD); \
		store_graph_to_disk__tree_node((tree_node*) VALUE); \
		fprintf(store_graph_to_disk__fd, "H %s.%s }\n", #TYPE, "$" #FIELD); \
	}
	
#endif


// 5 - contains property directly in field
#ifndef HANDLE_TREENODELAYOUT5
#define HANDLE_TREENODELAYOUT5(TYPE, FORMAT, FIELD) \
	\
	if(PASS == 1) \
		fprintf(store_graph_to_disk__fd, "I %s.%s [%s]: " FORMAT "\n", #TYPE, #FIELD, FORMAT, TNL_FIELD(TYPE, FIELD));
	
#endif

#ifndef HANDLE_TREENODELAYOUT5_STR
#define HANDLE_TREENODELAYOUT5_STR(TYPE, FIELD) \
	\
	if(PASS == 1) { \
		const char *tmp_str = TNL_FIELD(TYPE, FIELD); \
		size_t tmp_len = strlen(tmp_str); \
		for(int i = 0; i < tmp_len; i++) \
			fprintf(store_graph_to_disk__fd, "I %s.%s [%s]: " "%d" "\n", #TYPE, #FIELD "[i]", "%d", tmp_str[i]); \
	}
	
#endif


// 6 - contains property indirectly or not in field
#ifndef HANDLE_TREENODELAYOUT6
#define HANDLE_TREENODELAYOUT6(NAME, FORMAT, VALUE) \
	\
	if(PASS == 1) \
		fprintf(store_graph_to_disk__fd, "J %s [%s]: " FORMAT "\n", #NAME, FORMAT, VALUE);
	
#endif


// 7 - contains property at offset
#ifndef HANDLE_TREENODELAYOUT7
#define HANDLE_TREENODELAYOUT7(TYPE, FORMAT, SIZE, OFFSET, FIELD) \
	\
	SIZE FIELD = * (SIZE*) ((void*) t + OFFSET); \
	HANDLE_TREENODELAYOUT6(TYPE.FIELD, FORMAT, FIELD)
	
#endif


#ifndef HANDLE_TREECODE
#define HANDLE_TREECODE(SELECTOR, LEN, TCC, TNL) \
	\
	if(TREE_CODE(t) == SELECTOR) { \
		store_graph_to_disk__tree_node2(t, SELECTOR, #SELECTOR, LEN, TCC, #TCC, TNL, #TNL); \
		return; \
	}
	
#endif


// TODO implement all tnl_handlers
// TODO implement variable length tnl


static void store_graph_to_disk__tree_node(tree_node* t);

TNL_HANDLER_SIGNATURE(tnl_base) {
	HANDLE_TREENODELAYOUT7(tree_base, "%04x", unsigned short, 0, code)
	HANDLE_TREENODELAYOUT7(tree_base, "%04x", unsigned short, 2, flags)
	HANDLE_TREENODELAYOUT7(tree_base, "%08x", unsigned int, 4, special)
}

TNL_HANDLER_SIGNATURE(tnl_typed) {
	HANDLE_TREENODELAYOUT2(tnl_base)
	HANDLE_TREENODELAYOUT3(tree_typed, type)
}

TNL_HANDLER_SIGNATURE(tnl_common) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	//HANDLE_TREENODELAYOUT3(tree_common, chain) // TODO check if required
}

TNL_HANDLER_SIGNATURE(tnl_int_cst) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	for(int i = 0; i < LEN; i++)
		HANDLE_TREENODELAYOUT5(tree_int_cst, "%d", val[i]) // TODO check
}


TNL_HANDLER_SIGNATURE(tnl_real_cst) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	HANDLE_TREENODELAYOUT6(tree_real_cst.real_cst_ptr, "%016llx", * (unsigned long long*) TNL_FIELD(tree_real_cst, real_cst_ptr))
}

TNL_HANDLER_SIGNATURE(tnl_fixed_cst) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	HANDLE_TREENODELAYOUT6(tree_fixed_cst.fixed_cst_ptr, "%016llx", * (unsigned long long*) TNL_FIELD(tree_fixed_cst, fixed_cst_ptr)) // TODO check
}

TNL_HANDLER_SIGNATURE(tnl_string) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	HANDLE_TREENODELAYOUT5(tree_string, "%d", length)
	for(int i = 0; i < TNL_FIELD(tree_string, length); i++)
		HANDLE_TREENODELAYOUT5(tree_string, "%d", str[i])
}

TNL_HANDLER_SIGNATURE(tnl_complex) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	HANDLE_TREENODELAYOUT3(tree_complex, real)
	HANDLE_TREENODELAYOUT3(tree_complex, imag)
}

TNL_HANDLER_SIGNATURE(tnl_vector) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	for(int i = 0; i < LEN; i++)
		HANDLE_TREENODELAYOUT3(tree_vector, elts[i])
}

TNL_HANDLER_SIGNATURE(tnl_poly_int_cst) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	for(int i = 0; i < NUM_POLY_INT_COEFFS; i++)
		HANDLE_TREENODELAYOUT3(tree_poly_int_cst, coeffs[i])
}

TNL_HANDLER_SIGNATURE(tnl_identifier) {
	HANDLE_TREENODELAYOUT2(tnl_common)
	HANDLE_TREENODELAYOUT6(tree_identifier.id.len, "%d", TNL_FIELD(tree_identifier, id).len)
	HANDLE_TREENODELAYOUT6(tree_identifier.id.hash_value, "%d", TNL_FIELD(tree_identifier, id).hash_value)
	for(int i = 0; i < TNL_FIELD(tree_identifier, id).len; i++)
		HANDLE_TREENODELAYOUT6(tree_identifier.id.str[i], "%d", TNL_FIELD(tree_identifier, id).str[i])
}

TNL_HANDLER_SIGNATURE(tnl_list) {
	HANDLE_TREENODELAYOUT2(tnl_common)
	HANDLE_TREENODELAYOUT3(tree_list, purpose)
	HANDLE_TREENODELAYOUT3(tree_list, value)
}

TNL_HANDLER_SIGNATURE(tnl_vec) {
	HANDLE_TREENODELAYOUT2(tnl_common)
	for(int i = 0; i < LEN; i++)
		HANDLE_TREENODELAYOUT3(tree_vec, a[i])
}

TNL_HANDLER_SIGNATURE(tnl_constructor) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	if(TNL_FIELD(tree_constructor, elts)) for(int i = 0; i < TNL_FIELD(tree_constructor, elts)->length(); i++) {
		HANDLE_TREENODELAYOUT4(tree_constructor.elts[i], index, TNL_FIELD(tree_constructor, elts)->begin()[i].index)
		HANDLE_TREENODELAYOUT4(tree_constructor.elts[i], value, TNL_FIELD(tree_constructor, elts)->begin()[i].value)
	}
}

TNL_HANDLER_SIGNATURE(tnl_exp) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	HANDLE_TREENODELAYOUT5(tree_exp, "%d", locus)
	for(int i = 0; i < LEN; i++)
		HANDLE_TREENODELAYOUT3(tree_exp, operands[i])
}

TNL_HANDLER_SIGNATURE(tnl_ssa_name) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	HANDLE_TREENODELAYOUT3(tree_ssa_name, var)
	
	// TODO check if required
	
	/*struct tree_typed typed;
	tree var;
	gimple *def_stmt;
	
	union ssa_name_info_type {
	struct ptr_info_def *ptr_info;
	struct range_info_def *range_info;
	} info;
	
	struct ssa_use_operand_t imm_uses;*/
}

TNL_HANDLER_SIGNATURE(tnl_omp_clause) {
	HANDLE_TREENODELAYOUT2(tnl_common)
	HANDLE_TREENODELAYOUT5(tree_omp_clause, "%d", locus)
	for(int i = 0; i < LEN; i++)
		HANDLE_TREENODELAYOUT3(tree_omp_clause, ops[i])
	
	// TODO check if required
	
	/*struct tree_common common;
	location_t locus;
	enum omp_clause_code code;
	union omp_clause_subcode {
	enum omp_clause_default_kind default_kind;
	enum omp_clause_schedule_kind schedule_kind;
	enum omp_clause_depend_kind depend_kind;
	unsigned int map_kind;
	enum omp_clause_proc_bind_kind proc_bind_kind;
	enum tree_code reduction_code;
	enum omp_clause_linear_kind linear_kind;
	enum tree_code if_modifier;
	enum omp_clause_defaultmap_kind defaultmap_kind;
	enum omp_clause_bind_kind bind_kind;
	enum omp_clause_device_type_kind device_type_kind;
	} subcode;
	
	gimple_seq gimple_reduction_init;
	gimple_seq gimple_reduction_merge;
	
	tree ops[1];*/
}

TNL_HANDLER_SIGNATURE(tnl_block) {
	HANDLE_TREENODELAYOUT2(tnl_base)
	HANDLE_TREENODELAYOUT3(tree_block, chain)
	HANDLE_TREENODELAYOUT5(tree_block, "%d", block_num)
	HANDLE_TREENODELAYOUT5(tree_block, "%d", locus)
	HANDLE_TREENODELAYOUT5(tree_block, "%d", end_locus)
	HANDLE_TREENODELAYOUT3(tree_block, vars)
	if(TNL_FIELD(tree_block, nonlocalized_vars)) for(int i = 0; i < TNL_FIELD(tree_block, nonlocalized_vars)->length(); i++) {
		HANDLE_TREENODELAYOUT4(tree_block, nonlocalized_vars[i], TNL_FIELD(tree_block, nonlocalized_vars)->begin()[i])
	}
	HANDLE_TREENODELAYOUT3(tree_block, subblocks)
	HANDLE_TREENODELAYOUT3(tree_block, supercontext)
	HANDLE_TREENODELAYOUT3(tree_block, abstract_origin)
	HANDLE_TREENODELAYOUT3(tree_block, fragment_origin)
	HANDLE_TREENODELAYOUT3(tree_block, fragment_chain)
	HANDLE_TREENODELAYOUT5(tree_block, "%016llx", die) // TODO check if required
	
	
	// TODO check if required
	
	/*
	struct die_struct *die;*/
}

TNL_HANDLER_SIGNATURE(tnl_type_common) {
	HANDLE_TREENODELAYOUT2(tnl_common)
	
	HANDLE_TREENODELAYOUT3(tree_type_common, size)
	HANDLE_TREENODELAYOUT3(tree_type_common, size_unit)
	HANDLE_TREENODELAYOUT3(tree_type_common, attributes)
	HANDLE_TREENODELAYOUT5(tree_type_common, "%d", uid)
	HANDLE_TREENODELAYOUT7(tree_type_common, "%016llx", unsigned long long, sizeof(struct tree_common) + sizeof(void*) * 3 + sizeof(unsigned int), flags)
	HANDLE_TREENODELAYOUT5(tree_type_common, "%d", alias_set)
	HANDLE_TREENODELAYOUT3(tree_type_common, pointer_to)
	HANDLE_TREENODELAYOUT3(tree_type_common, reference_to)
	HANDLE_TREENODELAYOUT6(tree_type_common.symtab.die, "%016llx", TNL_FIELD(tree_type_common, symtab).die) // TODO check if required
	HANDLE_TREENODELAYOUT3(tree_type_common, canonical)
	HANDLE_TREENODELAYOUT3(tree_type_common, next_variant)
	HANDLE_TREENODELAYOUT3(tree_type_common, main_variant)
	HANDLE_TREENODELAYOUT3(tree_type_common, context)
	HANDLE_TREENODELAYOUT3(tree_type_common, name)
}

TNL_HANDLER_SIGNATURE(tnl_type_with_lang_specific) {
	HANDLE_TREENODELAYOUT2(tnl_type_common)
	
	// TODO check if required
	
	/*
	struct lang_type *lang_specific;*/
}

TNL_HANDLER_SIGNATURE(tnl_type_non_common) {
	/*struct tree_type_with_lang_specific with_lang_specific;
	tree values;
	tree minval;
	tree maxval;
	tree lang_1;*/
}

TNL_HANDLER_SIGNATURE(tnl_binfo) {
	HANDLE_TREENODELAYOUT2(tnl_common)
	/*struct tree_common common;
	
	tree offset;
	tree vtable;
	tree virtuals;
	tree vptr_field;
	vec<tree, va_gc> *base_accesses;
	tree inheritance;
	
	tree vtt_subvtt;
	tree vtt_vptr;
	
	vec<tree, va_gc> base_binfos;*/
}

TNL_HANDLER_SIGNATURE(tnl_decl_minimal) {
	HANDLE_TREENODELAYOUT2(tnl_common)
	HANDLE_TREENODELAYOUT5(tree_decl_minimal, "%d", locus)
	HANDLE_TREENODELAYOUT5(tree_decl_minimal, "%d", uid)
	HANDLE_TREENODELAYOUT3(tree_decl_minimal, name)
	HANDLE_TREENODELAYOUT3(tree_decl_minimal, context)
}

TNL_HANDLER_SIGNATURE(tnl_decl_common) {
	HANDLE_TREENODELAYOUT2(tnl_decl_minimal)
	HANDLE_TREENODELAYOUT3(tree_decl_common, size)
	HANDLE_TREENODELAYOUT7(tree_decl_common, "%014llx", unsigned long long, sizeof(struct tree_decl_minimal) + sizeof(void*), flags)
	HANDLE_TREENODELAYOUT5(tree_decl_common, "%d", pt_uid)
	HANDLE_TREENODELAYOUT3(tree_decl_common, size_unit)
	HANDLE_TREENODELAYOUT3(tree_decl_common, initial)
	HANDLE_TREENODELAYOUT3(tree_decl_common, attributes)
	HANDLE_TREENODELAYOUT3(tree_decl_common, abstract_origin)
	
	// TODO check if required
	
	/*
	struct lang_decl *lang_specific;*/
}

TNL_HANDLER_SIGNATURE(tnl_decl_with_rtl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_common)
	
	// TODO check if required
	
	/*
	rtx rtl;*/
}

TNL_HANDLER_SIGNATURE(tnl_field_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_common)
	HANDLE_TREENODELAYOUT3(tree_field_decl, offset)
	HANDLE_TREENODELAYOUT3(tree_field_decl, bit_field_type)
	HANDLE_TREENODELAYOUT3(tree_field_decl, qualifier)
	HANDLE_TREENODELAYOUT3(tree_field_decl, bit_offset)
	HANDLE_TREENODELAYOUT3(tree_field_decl, fcontext)
}

TNL_HANDLER_SIGNATURE(tnl_label_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_with_rtl)
	HANDLE_TREENODELAYOUT5(tree_label_decl, "%d", label_decl_uid)
	HANDLE_TREENODELAYOUT5(tree_label_decl, "%d", eh_landing_pad_nr)
}

TNL_HANDLER_SIGNATURE(tnl_result_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_with_rtl)
}

TNL_HANDLER_SIGNATURE(tnl_const_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_common)
}

TNL_HANDLER_SIGNATURE(tnl_parm_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_with_rtl)
	
	// TODO check if required
	
	/*
	rtx incoming_rtl;*/
}

TNL_HANDLER_SIGNATURE(tnl_decl_with_vis) {
	HANDLE_TREENODELAYOUT2(tnl_decl_with_rtl)
	HANDLE_TREENODELAYOUT3(tree_decl_with_vis, assembler_name)
	
	HANDLE_TREENODELAYOUT7(tree_decl_with_vis, "%06lx", unsigned long, sizeof(struct tree_decl_with_rtl) + sizeof(void*) * 2, flags)
	
	// TODO check if required
	
	/*
	struct symtab_node *symtab_node;*/
}

TNL_HANDLER_SIGNATURE(tnl_var_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_with_vis)
}

TNL_HANDLER_SIGNATURE(tnl_decl_non_common) {
	HANDLE_TREENODELAYOUT2(tnl_decl_with_vis)
	//HANDLE_TREENODELAYOUT3(tree_decl_non_common, result) // TODO crashes
}

TNL_HANDLER_SIGNATURE(tnl_function_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_non_common)
	
	HANDLE_TREENODELAYOUT3(tree_function_decl, arguments)
	HANDLE_TREENODELAYOUT3(tree_function_decl, personality)
	HANDLE_TREENODELAYOUT3(tree_function_decl, function_specific_target)
	HANDLE_TREENODELAYOUT3(tree_function_decl, function_specific_optimization)
	HANDLE_TREENODELAYOUT3(tree_function_decl, saved_tree)
	HANDLE_TREENODELAYOUT3(tree_function_decl, vindex)
	HANDLE_TREENODELAYOUT5(tree_function_decl, "%d", function_code)
	HANDLE_TREENODELAYOUT7(tree_function_decl, "%06lx", unsigned long, sizeof(struct tree_decl_non_common) + sizeof(void*) * 7 + sizeof(unsigned int), flags)
	
	// TODO check if required
	
	/*
	struct function *f;*/
}

TNL_HANDLER_SIGNATURE(tnl_translation_unit_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_common)
	//HANDLE_TREENODELAYOUT5(tree_translation_unit_decl, "%s", language)
	HANDLE_TREENODELAYOUT5_STR(tree_translation_unit_decl, language)
}

TNL_HANDLER_SIGNATURE(tnl_type_decl) {
	HANDLE_TREENODELAYOUT2(tnl_decl_non_common)
}

TNL_HANDLER_SIGNATURE(tnl_statement_list) {
	HANDLE_TREENODELAYOUT2(tnl_typed)
	if(TNL_FIELD(tree_statement_list, head)) for(struct tree_statement_list_node *it = TNL_FIELD(tree_statement_list, head); it; it = it->next) {
		HANDLE_TREENODELAYOUT4(tree_statement_list, nodes[i], it->stmt)
	}
}

TNL_HANDLER_SIGNATURE(tnl_optimization_option) {
	HANDLE_TREENODELAYOUT2(tnl_base)
	/*struct tree_base base;
	struct cl_optimization *opts;
	void *optabs;
	struct target_optabs *base_optabs;*/
}

TNL_HANDLER_SIGNATURE(tnl_target_option) {
	HANDLE_TREENODELAYOUT2(tnl_base)
	/*struct tree_base base;
	class target_globals *globals;
	struct cl_target_option *opts;*/
}


static void store_graph_to_disk__tree_node2(tree_node* t, unsigned int SELECTOR, const char* _SELECTOR, unsigned int LEN, unsigned int TCC, const char* _TCC, enum store_graph_to_disk__tree_node_layout TNL, const char* _TNL) {
	
	fprintf(store_graph_to_disk__fd, "C SELECTOR %d %s  LEN %d  TCC %d %s  TNL %d %s  (%016llx)\n", SELECTOR, _SELECTOR, LEN, TCC, _TCC, TNL, _TNL, t);
	
	
	// store properties according to store_graph_to_disk__tree_node_layout
	
	HANDLE_TREENODELAYOUT(1,tnl_base);
	HANDLE_TREENODELAYOUT(1,tnl_typed);
	HANDLE_TREENODELAYOUT(1,tnl_common);
	HANDLE_TREENODELAYOUT(1,tnl_int_cst);
	HANDLE_TREENODELAYOUT(1,tnl_poly_int_cst);
	HANDLE_TREENODELAYOUT(1,tnl_real_cst);
	HANDLE_TREENODELAYOUT(1,tnl_fixed_cst);
	HANDLE_TREENODELAYOUT(1,tnl_vector);
	HANDLE_TREENODELAYOUT(1,tnl_string);
	HANDLE_TREENODELAYOUT(1,tnl_complex);
	HANDLE_TREENODELAYOUT(1,tnl_identifier);
	HANDLE_TREENODELAYOUT(1,tnl_decl_minimal);
	HANDLE_TREENODELAYOUT(1,tnl_decl_common);
	HANDLE_TREENODELAYOUT(1,tnl_decl_with_rtl);
	HANDLE_TREENODELAYOUT(1,tnl_decl_non_common);
	HANDLE_TREENODELAYOUT(1,tnl_parm_decl);
	HANDLE_TREENODELAYOUT(1,tnl_decl_with_vis);
	HANDLE_TREENODELAYOUT(1,tnl_var_decl);
	HANDLE_TREENODELAYOUT(1,tnl_field_decl);
	HANDLE_TREENODELAYOUT(1,tnl_label_decl);
	HANDLE_TREENODELAYOUT(1,tnl_result_decl);
	HANDLE_TREENODELAYOUT(1,tnl_const_decl);
	HANDLE_TREENODELAYOUT(1,tnl_type_decl);
	HANDLE_TREENODELAYOUT(1,tnl_function_decl);
	HANDLE_TREENODELAYOUT(1,tnl_translation_unit_decl);
	HANDLE_TREENODELAYOUT(1,tnl_type_common);
	HANDLE_TREENODELAYOUT(1,tnl_type_with_lang_specific);
	HANDLE_TREENODELAYOUT(1,tnl_type_non_common);
	HANDLE_TREENODELAYOUT(1,tnl_list);
	HANDLE_TREENODELAYOUT(1,tnl_vec);
	HANDLE_TREENODELAYOUT(1,tnl_exp);
	HANDLE_TREENODELAYOUT(1,tnl_ssa_name);
	HANDLE_TREENODELAYOUT(1,tnl_block);
	HANDLE_TREENODELAYOUT(1,tnl_binfo);
	HANDLE_TREENODELAYOUT(1,tnl_statement_list);
	HANDLE_TREENODELAYOUT(1,tnl_constructor);
	HANDLE_TREENODELAYOUT(1,tnl_omp_clause);
	HANDLE_TREENODELAYOUT(1,tnl_optimization_option);
	HANDLE_TREENODELAYOUT(1,tnl_target_option);
	
	
	// depth-first graph search
	
	HANDLE_TREENODELAYOUT(2,tnl_base);
	HANDLE_TREENODELAYOUT(2,tnl_typed);
	HANDLE_TREENODELAYOUT(2,tnl_common);
	HANDLE_TREENODELAYOUT(2,tnl_int_cst);
	HANDLE_TREENODELAYOUT(2,tnl_poly_int_cst);
	HANDLE_TREENODELAYOUT(2,tnl_real_cst);
	HANDLE_TREENODELAYOUT(2,tnl_fixed_cst);
	HANDLE_TREENODELAYOUT(2,tnl_vector);
	HANDLE_TREENODELAYOUT(2,tnl_string);
	HANDLE_TREENODELAYOUT(2,tnl_complex);
	HANDLE_TREENODELAYOUT(2,tnl_identifier);
	HANDLE_TREENODELAYOUT(2,tnl_decl_minimal);
	HANDLE_TREENODELAYOUT(2,tnl_decl_common);
	HANDLE_TREENODELAYOUT(2,tnl_decl_with_rtl);
	HANDLE_TREENODELAYOUT(2,tnl_decl_non_common);
	HANDLE_TREENODELAYOUT(2,tnl_parm_decl);
	HANDLE_TREENODELAYOUT(2,tnl_decl_with_vis);
	HANDLE_TREENODELAYOUT(2,tnl_var_decl);
	HANDLE_TREENODELAYOUT(2,tnl_field_decl);
	HANDLE_TREENODELAYOUT(2,tnl_label_decl);
	HANDLE_TREENODELAYOUT(2,tnl_result_decl);
	HANDLE_TREENODELAYOUT(2,tnl_const_decl);
	HANDLE_TREENODELAYOUT(2,tnl_type_decl);
	HANDLE_TREENODELAYOUT(2,tnl_function_decl);
	HANDLE_TREENODELAYOUT(2,tnl_translation_unit_decl);
	HANDLE_TREENODELAYOUT(2,tnl_type_common);
	HANDLE_TREENODELAYOUT(2,tnl_type_with_lang_specific);
	HANDLE_TREENODELAYOUT(2,tnl_type_non_common);
	HANDLE_TREENODELAYOUT(2,tnl_list);
	HANDLE_TREENODELAYOUT(2,tnl_vec);
	HANDLE_TREENODELAYOUT(2,tnl_exp);
	HANDLE_TREENODELAYOUT(2,tnl_ssa_name);
	HANDLE_TREENODELAYOUT(2,tnl_block);
	HANDLE_TREENODELAYOUT(2,tnl_binfo);
	HANDLE_TREENODELAYOUT(2,tnl_statement_list);
	HANDLE_TREENODELAYOUT(2,tnl_constructor);
	HANDLE_TREENODELAYOUT(2,tnl_omp_clause);
	HANDLE_TREENODELAYOUT(2,tnl_optimization_option);
	HANDLE_TREENODELAYOUT(2,tnl_target_option);
	
}


static void store_graph_to_disk__tree_node(tree_node* t) {
	
	
	if(!t) return;
	
	if(store_graph_to_disk__ref.count(t)) {
		fprintf(store_graph_to_disk__fd, "D REF  (%016llx)\n", t);
		return;
	}
	store_graph_to_disk__ref.insert(t);
	
	
	// tree.def
	
	HANDLE_TREECODE( ERROR_MARK,                   0, tcc_exceptional,   tnl_common )
	HANDLE_TREECODE( IDENTIFIER_NODE,              0, tcc_exceptional,   tnl_identifier )
	HANDLE_TREECODE( TREE_LIST,                    0, tcc_exceptional,   tnl_list )
	HANDLE_TREECODE( TREE_VEC,                     0, tcc_exceptional,   tnl_vec )
	HANDLE_TREECODE( BLOCK,                        0, tcc_exceptional,   tnl_block )
	HANDLE_TREECODE( OFFSET_TYPE,                  0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( ENUMERAL_TYPE,                0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( BOOLEAN_TYPE,                 0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( INTEGER_TYPE,                 0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( REAL_TYPE,                    0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( POINTER_TYPE,                 0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( REFERENCE_TYPE,               0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( NULLPTR_TYPE,                 0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( FIXED_POINT_TYPE,             0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( COMPLEX_TYPE,                 0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( VECTOR_TYPE,                  0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( ARRAY_TYPE,                   0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( RECORD_TYPE,                  0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( UNION_TYPE,                   0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( QUAL_UNION_TYPE,              0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( VOID_TYPE,                    0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( FUNCTION_TYPE,                0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( METHOD_TYPE,                  0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( LANG_TYPE,                    0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( OPAQUE_TYPE,                  0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( VOID_CST,                     0, tcc_constant,      tnl_typed )
	HANDLE_TREECODE( INTEGER_CST,                  0, tcc_constant,      tnl_int_cst )
	HANDLE_TREECODE( POLY_INT_CST,                 0, tcc_constant,      tnl_poly_int_cst )
	HANDLE_TREECODE( REAL_CST,                     0, tcc_constant,      tnl_real_cst )
	HANDLE_TREECODE( FIXED_CST,                    0, tcc_constant,      tnl_fixed_cst )
	HANDLE_TREECODE( COMPLEX_CST,                  0, tcc_constant,      tnl_complex )
	HANDLE_TREECODE( VECTOR_CST,                   0, tcc_constant,      tnl_vector )
	HANDLE_TREECODE( STRING_CST,                   0, tcc_constant,      tnl_string )
	HANDLE_TREECODE( FUNCTION_DECL,                0, tcc_declaration,   tnl_function_decl )
	HANDLE_TREECODE( LABEL_DECL,                   0, tcc_declaration,   tnl_label_decl )
	HANDLE_TREECODE( FIELD_DECL,                   0, tcc_declaration,   tnl_field_decl )
	HANDLE_TREECODE( VAR_DECL,                     0, tcc_declaration,   tnl_var_decl )
	HANDLE_TREECODE( CONST_DECL,                   0, tcc_declaration,   tnl_const_decl )
	HANDLE_TREECODE( PARM_DECL,                    0, tcc_declaration,   tnl_parm_decl )
	HANDLE_TREECODE( TYPE_DECL,                    0, tcc_declaration,   tnl_type_decl )
	HANDLE_TREECODE( RESULT_DECL,                  0, tcc_declaration,   tnl_result_decl )
	HANDLE_TREECODE( DEBUG_EXPR_DECL,              0, tcc_declaration,   tnl_decl_with_rtl )
	HANDLE_TREECODE( DEBUG_BEGIN_STMT,             0, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( NAMESPACE_DECL,               0, tcc_declaration,   tnl_decl_non_common )
	HANDLE_TREECODE( IMPORTED_DECL,                0, tcc_declaration,   tnl_decl_non_common )
	HANDLE_TREECODE( NAMELIST_DECL,                0, tcc_declaration,   tnl_decl_non_common )
	HANDLE_TREECODE( TRANSLATION_UNIT_DECL,        0, tcc_declaration,   tnl_translation_unit_decl )
	HANDLE_TREECODE( COMPONENT_REF,                3, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( BIT_FIELD_REF,                3, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( ARRAY_REF,                    4, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( ARRAY_RANGE_REF,              4, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( REALPART_EXPR,                1, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( IMAGPART_EXPR,                1, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( VIEW_CONVERT_EXPR,            1, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( INDIRECT_REF,                 1, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( OBJ_TYPE_REF,                 3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CONSTRUCTOR,                  0, tcc_exceptional,   tnl_constructor )
	
	HANDLE_TREECODE( COMPOUND_EXPR,                2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( MODIFY_EXPR,                  2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( INIT_EXPR,                    2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TARGET_EXPR,                  4, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( COND_EXPR,                    3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( VEC_DUPLICATE_EXPR,           1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( VEC_SERIES_EXPR,              2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_COND_EXPR,                3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( VEC_PERM_EXPR,                3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( BIND_EXPR,                    3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CALL_EXPR,                    3, tcc_vl_exp,        tnl_exp )
	HANDLE_TREECODE( WITH_CLEANUP_EXPR,            1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CLEANUP_POINT_EXPR,           1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( PLACEHOLDER_EXPR,             0, tcc_exceptional,   tnl_common )
	HANDLE_TREECODE( PLUS_EXPR,                    2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( MINUS_EXPR,                   2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( MULT_EXPR,                    2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( POINTER_PLUS_EXPR,            2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( POINTER_DIFF_EXPR,            2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( MULT_HIGHPART_EXPR,           2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( TRUNC_DIV_EXPR,               2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( CEIL_DIV_EXPR,                2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( FLOOR_DIV_EXPR,               2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( ROUND_DIV_EXPR,               2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( TRUNC_MOD_EXPR,               2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( CEIL_MOD_EXPR,                2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( FLOOR_MOD_EXPR,               2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( ROUND_MOD_EXPR,               2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( RDIV_EXPR,                    2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( EXACT_DIV_EXPR,               2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( FIX_TRUNC_EXPR,               1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( FLOAT_EXPR,                   1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( NEGATE_EXPR,                  1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( MIN_EXPR,                     2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( MAX_EXPR,                     2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( ABS_EXPR,                     1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( ABSU_EXPR,                    1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( LSHIFT_EXPR,                  2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( RSHIFT_EXPR,                  2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( LROTATE_EXPR,                 2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( RROTATE_EXPR,                 2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( BIT_IOR_EXPR,                 2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( BIT_XOR_EXPR,                 2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( BIT_AND_EXPR,                 2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( BIT_NOT_EXPR,                 1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( TRUTH_ANDIF_EXPR,             2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TRUTH_ORIF_EXPR,              2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TRUTH_AND_EXPR,               2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TRUTH_OR_EXPR,                2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TRUTH_XOR_EXPR,               2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TRUTH_NOT_EXPR,               1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( LT_EXPR,                      2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( LE_EXPR,                      2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( GT_EXPR,                      2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( GE_EXPR,                      2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( LTGT_EXPR,                    2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( EQ_EXPR,                      2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( NE_EXPR,                      2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( UNORDERED_EXPR,               2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( ORDERED_EXPR,                 2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( UNLT_EXPR,                    2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( UNLE_EXPR,                    2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( UNGT_EXPR,                    2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( UNGE_EXPR,                    2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( UNEQ_EXPR,                    2, tcc_comparison,    tnl_exp )
	HANDLE_TREECODE( RANGE_EXPR,                   2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( PAREN_EXPR,                   1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( CONVERT_EXPR,                 1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( ADDR_SPACE_CONVERT_EXPR,      1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( FIXED_CONVERT_EXPR,           1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( NOP_EXPR,                     1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( NON_LVALUE_EXPR,              1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( COMPOUND_LITERAL_EXPR,        1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( SAVE_EXPR,                    1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( ADDR_EXPR,                    1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( FDESC_EXPR,                   2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( BIT_INSERT_EXPR,              3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( COMPLEX_EXPR,                 2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( CONJ_EXPR,                    1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( PREDECREMENT_EXPR,            2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( PREINCREMENT_EXPR,            2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( POSTDECREMENT_EXPR,           2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( POSTINCREMENT_EXPR,           2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( VA_ARG_EXPR,                  1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TRY_CATCH_EXPR,               2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( TRY_FINALLY_EXPR,             2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( EH_ELSE_EXPR,                 2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( DECL_EXPR,                    1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( LABEL_EXPR,                   1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( GOTO_EXPR,                    1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( RETURN_EXPR,                  1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( EXIT_EXPR,                    1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( LOOP_EXPR,                    1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( SWITCH_EXPR,                  2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( CASE_LABEL_EXPR,              4, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( ASM_EXPR,                     5, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( SSA_NAME,                     0, tcc_exceptional,   tnl_ssa_name )
	HANDLE_TREECODE( CATCH_EXPR,                   2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( EH_FILTER_EXPR,               2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( SCEV_KNOWN,                   0, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( SCEV_NOT_KNOWN,               0, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( POLYNOMIAL_CHREC,             2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( STATEMENT_LIST,               0, tcc_exceptional,   tnl_statement_list )
	HANDLE_TREECODE( ASSERT_EXPR,                  2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TREE_BINFO,                   0, tcc_exceptional,   tnl_binfo )
	HANDLE_TREECODE( WITH_SIZE_EXPR,               2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( REALIGN_LOAD_EXPR,            3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TARGET_MEM_REF,               5, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( MEM_REF,                      2, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( OACC_PARALLEL,                2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_KERNELS,                 2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_SERIAL,                  2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_DATA,                    2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_HOST_DATA,               2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_PARALLEL,                 2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TASK,                     2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_FOR,                      7, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_SIMD,                     7, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_DISTRIBUTE,               7, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TASKLOOP,                 7, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_LOOP,                     7, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_LOOP,                    7, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TEAMS,                    2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TARGET_DATA,              2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TARGET,                   2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_SECTIONS,                 2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_ORDERED,                  2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_CRITICAL,                 3, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_SINGLE,                   2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_SCOPE,                    2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TASKGROUP,                2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_MASKED,                   2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_SCAN,                     2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_SECTION,                  1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_MASTER,                   1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_CACHE,                   1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_DECLARE,                 1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_ENTER_DATA,              1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_EXIT_DATA,               1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OACC_UPDATE,                  1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TARGET_UPDATE,            1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TARGET_ENTER_DATA,        1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_TARGET_EXIT_DATA,         1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_ATOMIC,                   2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_ATOMIC_READ,              1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_ATOMIC_CAPTURE_OLD,       2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_ATOMIC_CAPTURE_NEW,       2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( OMP_CLAUSE,                   0, tcc_exceptional,   tnl_omp_clause )
	HANDLE_TREECODE( TRANSACTION_EXPR,             1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( DOT_PROD_EXPR,                3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( WIDEN_SUM_EXPR,               2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( SAD_EXPR,                     3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( WIDEN_MULT_EXPR,              2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( WIDEN_MULT_PLUS_EXPR,         3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( WIDEN_MULT_MINUS_EXPR,        3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( WIDEN_LSHIFT_EXPR,            2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( WIDEN_PLUS_EXPR,              2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( WIDEN_MINUS_EXPR,             2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_MULT_HI_EXPR,       2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_MULT_LO_EXPR,       2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_MULT_EVEN_EXPR,     2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_MULT_ODD_EXPR,      2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_UNPACK_HI_EXPR,           1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( VEC_UNPACK_LO_EXPR,           1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( VEC_UNPACK_FLOAT_HI_EXPR,     1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( VEC_UNPACK_FLOAT_LO_EXPR,     1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( VEC_UNPACK_FIX_TRUNC_HI_EXPR, 1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( VEC_UNPACK_FIX_TRUNC_LO_EXPR, 1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( VEC_PACK_TRUNC_EXPR,          2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_PACK_SAT_EXPR,            2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_PACK_FIX_TRUNC_EXPR,      2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_PACK_FLOAT_EXPR,          2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_LSHIFT_HI_EXPR,     2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_LSHIFT_LO_EXPR,     2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_PLUS_HI_EXPR,       2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_PLUS_LO_EXPR,       2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_MINUS_HI_EXPR,      2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( VEC_WIDEN_MINUS_LO_EXPR,      2, tcc_binary,        tnl_exp )
	HANDLE_TREECODE( PREDICT_EXPR,                 1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( OPTIMIZATION_NODE,            0, tcc_exceptional,   tnl_optimization_option )
	HANDLE_TREECODE( TARGET_OPTION_NODE,           0, tcc_exceptional,   tnl_target_option )
	HANDLE_TREECODE( ANNOTATE_EXPR,                3, tcc_expression,    tnl_exp )
	
	// c-family/c-common.def
	
	HANDLE_TREECODE( C_MAYBE_CONST_EXPR,           2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( EXCESS_PRECISION_EXPR,        1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( USERDEF_LITERAL,              3, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( SIZEOF_EXPR,                  1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( PAREN_SIZEOF_EXPR,            1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( FOR_STMT,                     5, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( WHILE_STMT,                   2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( DO_STMT,                      2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( BREAK_STMT,                   0, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( CONTINUE_STMT,                0, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( SWITCH_STMT,                  4, tcc_statement,     tnl_exp )
	
	// cp/cp-tree.def
	
	HANDLE_TREECODE( OFFSET_REF,                   2, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( PTRMEM_CST,                   0, tcc_constant,      tnl_base ) // unspecified
	HANDLE_TREECODE( NEW_EXPR,                     4, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( VEC_NEW_EXPR,                 3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( DELETE_EXPR,                  2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( VEC_DELETE_EXPR,              2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( SCOPE_REF,                    2, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( MEMBER_REF,                   2, tcc_reference,     tnl_exp )
	HANDLE_TREECODE( TYPE_EXPR,                    1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( AGGR_INIT_EXPR,               3, tcc_vl_exp,        tnl_exp )
	HANDLE_TREECODE( VEC_INIT_EXPR,                2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( THROW_EXPR,                   1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( EMPTY_CLASS_EXPR,             0, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( BASELINK,                     0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( TEMPLATE_DECL,                0, tcc_declaration,   tnl_decl_non_common )
	HANDLE_TREECODE( TEMPLATE_PARM_INDEX,          0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( TEMPLATE_TEMPLATE_PARM,       0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( TEMPLATE_TYPE_PARM,           0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( TYPENAME_TYPE,                0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( TYPEOF_TYPE,                  0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( BOUND_TEMPLATE_TEMPLATE_PARM, 0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( UNBOUND_CLASS_TEMPLATE,       0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( USING_DECL,                   0, tcc_declaration,   tnl_decl_non_common )
	HANDLE_TREECODE( USING_STMT,                   1, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( DEFERRED_PARSE,               0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( DEFERRED_NOEXCEPT,            0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( TEMPLATE_ID_EXPR,             2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( OVERLOAD,                     0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( BINDING_VECTOR,               0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( PSEUDO_DTOR_EXPR,             3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( MODOP_EXPR,                   3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CAST_EXPR,                    1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( REINTERPRET_CAST_EXPR,        1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( CONST_CAST_EXPR,              1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( STATIC_CAST_EXPR,             1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( DYNAMIC_CAST_EXPR,            1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( IMPLICIT_CONV_EXPR,           1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( DOTSTAR_EXPR,                 2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TYPEID_EXPR,                  1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( NOEXCEPT_EXPR,                1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( SPACESHIP_EXPR,               2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( NON_DEPENDENT_EXPR,           1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CTOR_INITIALIZER,             1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TRY_BLOCK,                    2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( EH_SPEC_BLOCK,                2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( HANDLER,                      2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( MUST_NOT_THROW_EXPR,          2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CLEANUP_STMT,                 3, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( IF_STMT,                      4, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( RANGE_FOR_STMT,               6, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( EXPR_STMT,                    1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TAG_DEFN,                     0, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( OFFSETOF_EXPR,                2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( ADDRESSOF_EXPR,               1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( ARROW_EXPR,                   1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( ALIGNOF_EXPR,                 1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( AT_ENCODE_EXPR,               1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( STMT_EXPR,                    1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( UNARY_PLUS_EXPR,              1, tcc_unary,         tnl_exp )
	HANDLE_TREECODE( STATIC_ASSERT,                0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( TYPE_ARGUMENT_PACK,           0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( NONTYPE_ARGUMENT_PACK,        1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TYPE_PACK_EXPANSION,          0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( EXPR_PACK_EXPANSION,          3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( ARGUMENT_PACK_SELECT,         0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( UNARY_LEFT_FOLD_EXPR,         2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( UNARY_RIGHT_FOLD_EXPR,        2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( BINARY_LEFT_FOLD_EXPR,        3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( BINARY_RIGHT_FOLD_EXPR,       3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( BIT_CAST_EXPR,                1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TRAIT_EXPR,                   0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( LAMBDA_EXPR,                  0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( DECLTYPE_TYPE,                0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( UNDERLYING_TYPE,              0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( BASES,                        0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( DEPENDENT_OPERATOR_TYPE,      0, tcc_type,          tnl_type_non_common )
	HANDLE_TREECODE( TEMPLATE_INFO,                0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( OMP_DEPOBJ,                   2, tcc_statement,     tnl_exp )
	HANDLE_TREECODE( CONCEPT_DECL,                 0, tcc_declaration,   tnl_decl_non_common )
	HANDLE_TREECODE( CONSTRAINT_INFO,              0, tcc_exceptional,   tnl_base ) // unspecified
	HANDLE_TREECODE( WILDCARD_DECL,                0, tcc_declaration,   tnl_decl_non_common )
	HANDLE_TREECODE( REQUIRES_EXPR,                3, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( SIMPLE_REQ,                   1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( TYPE_REQ,                     1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( COMPOUND_REQ,                 2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( NESTED_REQ,                   1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( ATOMIC_CONSTR,                1, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CONJ_CONSTR,                  2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( DISJ_CONSTR,                  2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CHECK_CONSTR,                 2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CO_AWAIT_EXPR,                5, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CO_YIELD_EXPR,                2, tcc_expression,    tnl_exp )
	HANDLE_TREECODE( CO_RETURN_EXPR,               2, tcc_statement,     tnl_exp )
	
	
	fprintf(store_graph_to_disk__fd, "Z UNHANDLED TREECODE %d  (%016llx)\n", TREE_CODE(t), t);
}


static void store_graph_to_disk(struct symbol_table* s) {
	
	if(!getenv("STORE_GRAPH_TO_DISK")) return;
	
	system("mkdir /dev/shm/tmp");
	
	std::string tmpnam_ = "/dev/shm";
	tmpnam_ += std::tmpnam(NULL);
	store_graph_to_disk__fd = fopen(tmpnam_.c_str(), "w");
	
	
	fprintf(store_graph_to_disk__fd, "A symbol_table {\n");
	
	{
		unsigned int PASS = 1;
		struct symbol_table *t = s;
		
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", cgraph_count)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", cgraph_max_uid)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", cgraph_max_summary_id)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", edges_count)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", edges_max_uid)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", edges_max_summary_id)
		
		HANDLE_TREENODELAYOUT5(symbol_table, "%02x", symbol_suffix_separator)
		
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", order)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", max_unit)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", global_info_ready)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", state)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", function_flags_ready)
		HANDLE_TREENODELAYOUT5(symbol_table, "%d", cpp_implicit_aliases_done)
		
		// TODO check if required
		
		/*
		vec<int> cgraph_released_summary_ids;
		vec<int> edge_released_summary_ids;
		
		symtab_node *nodes;
		asm_node *asmnodes;
		asm_node *asm_last_node;
		
		hash_table<section_name_hasher> *section_hash;
		hash_table<asmname_hasher> *assembler_name_hash;
		hash_map<symtab_node *, symbol_priority_map> *init_priority_hash;
		
		hash_set <const cgraph_node *> cloned_nodes;
		thunk_summary *m_thunks;
		clone_summary *m_clones;
		*/
	}
	
	
	for(struct symtab_node *node = s->nodes; node; node = node->next) {
		
		fprintf(store_graph_to_disk__fd, "B symtab_node {\n");
		
		{
			struct symtab_node *t = node;
			
			unsigned int PASS = 1;
			
			HANDLE_TREENODELAYOUT7(symtab_node, "%02x", unsigned char, 0, type)
			HANDLE_TREENODELAYOUT7(symtab_node, "%02x", unsigned char, 1, resolution)
			HANDLE_TREENODELAYOUT7(symtab_node, "%06x", unsigned int, 2, flags)
			
			HANDLE_TREENODELAYOUT5(symtab_node, "%d", order)
			
			// TODO check if required
			
			/*
			symtab_node *next;
			symtab_node *previous;
			symtab_node *next_sharing_asm_name;
			symtab_node *previous_sharing_asm_name;
			symtab_node *same_comdat_group;
			
			ipa_ref_list ref_list;
			tree alias_target;
			struct lto_file_decl_data * lto_file_data;
			PTR aux;
			tree x_comdat_group;
			section_hash_entry *x_section;
			*/
			
			HANDLE_TREENODELAYOUT5_STR(symtab_node, name())
			HANDLE_TREENODELAYOUT5_STR(symtab_node, dump_name())
			HANDLE_TREENODELAYOUT5_STR(symtab_node, asm_name())
			HANDLE_TREENODELAYOUT5_STR(symtab_node, dump_asm_name())
			HANDLE_TREENODELAYOUT5_STR(symtab_node, get_visibility_string())
			HANDLE_TREENODELAYOUT5_STR(symtab_node, get_symtab_type_string())
			//HANDLE_TREENODELAYOUT5_STR(symtab_node, get_section())
			
			PASS = 2;
			
			HANDLE_TREENODELAYOUT3(symtab_node, decl)
		}
		
		fprintf(store_graph_to_disk__fd, "X symtab_node }\n");
	}
	
	fprintf(store_graph_to_disk__fd, "Y symbol_table }\n");
	
	
	fclose(store_graph_to_disk__fd);
	
	
	if(strcmp(getenv("STORE_GRAPH_TO_DISK"), "PRINT")) return;
	
	system(("flock /dev/shm/tmp/lock -c 'cat " + tmpnam_ + " >&3'").c_str());
	system(("rm " + tmpnam_).c_str());
}

