

// javac structurize.java -Xlint && javac pretty2.java -Xlint
// export SERIALIZATION_PROTOCOL=1; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | java -Xms8g -Xmx8g -Xss1g Structurize | hd


import java.util.LinkedList;
import java.util.LinkedHashMap;
import java.util.Stack;


class Structurize {
	
	
	static class State {
		static LinkedList<LinkedHashMap<String, Object>> symbol_tables = new LinkedList<LinkedHashMap<String, Object>>();
		static LinkedList<LinkedHashMap<String, Object>> symtab_nodes;
		static Stack<LinkedHashMap<String, Object>> tree_node_stack;
		static LinkedHashMap<String, Object> current;
	}
	
	static LinkedHashMap<String, LinkedHashMap<String, Object>> refs = new LinkedHashMap<String, LinkedHashMap<String, Object>>();
	
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws java.io.IOException {
		
		
		java.util.HashMap<String, java.util.function.Consumer<String[]>> handler = new java.util.HashMap<String, java.util.function.Consumer<String[]>>();
		handler.put("A", line -> {
			// pop/push symbol_table
			
			State.symbol_tables.add(new LinkedHashMap<String, Object>());
			State.current = State.symbol_tables.getLast();
			State.symtab_nodes = null;
			State.tree_node_stack = new Stack<LinkedHashMap<String, Object>>();
			State.tree_node_stack.push(State.current);
			
		});
		handler.put("B", line -> {
			// pop/push symtab_node
			
			if(State.symtab_nodes == null) { // append to end
				State.symtab_nodes = new LinkedList<LinkedHashMap<String, Object>>();
				State.current.put("symbol_table.$nodes", State.symtab_nodes);
			}
			
			State.symtab_nodes.add(new LinkedHashMap<String, Object>());
			State.current = State.symtab_nodes.getLast();
			State.tree_node_stack = new Stack<LinkedHashMap<String, Object>>();
			State.tree_node_stack.push(State.current);
			
		});
		handler.put("C", line -> {
			// push tree_node, step 2
			//
			// printf("C SELECTOR %d %s  LEN %d  TCC %d %s  TNL %d %s  (%016llx)\n", SELECTOR, _SELECTOR, LEN, TCC, _TCC, TNL, _TNL, t)
			
			State.current = State.tree_node_stack.peek();
			
			State.current.put(".selector", new LinkedList<String>(java.util.Arrays.asList(line[3], "%s")));
			State.current.put(".len", new LinkedList<String>(java.util.Arrays.asList(line[5], "%d")));
			State.current.put(".tcc", new LinkedList<String>(java.util.Arrays.asList(line[8], "%s")));
			State.current.put(".tnl", new LinkedList<String>(java.util.Arrays.asList(line[11], "%s")));
			State.current.put(".addr", new LinkedList<String>(java.util.Arrays.asList(line[12].substring(1, line[12].length() - 1), "%x")));
			
			refs.put(((LinkedList<String>) State.current.get(".addr")).get(0), State.current);
			
		});
		handler.put("D", line -> {
			// push tree_node, step 2 (reference to other tree_node)
			//
			// printf("D REF  (%016llx)\n", t)
			
			//State.current.addAll(refs(line[2].substring(1, line[2].length() - 1)))
			//for(x <- refs(line[2].substring(1, line[2].length() - 1))) State.current += x
			State.current.put(".ref", line[2].substring(1, line[2].length() - 1));
			
		});
		handler.put("E", line -> {
			// push tree_node, step 1 (node in field)
			//
			// 3 - contains node in field
			// HANDLE_TREENODELAYOUT3(TYPE, FIELD)
			// printf("E %s.%s {\n", #TYPE, #FIELD)
			
			if(line[1].contains("[")) {
				LinkedList<LinkedHashMap<String, Object>> list = new LinkedList<LinkedHashMap<String, Object>>();
				if(State.current.containsKey(line[1]))
					list = (LinkedList<LinkedHashMap<String, Object>>) State.current.get(line[1]);
				else
					State.current.put(line[1], list);
				State.current = new LinkedHashMap<String, Object>();
				list.add(State.current);
			}
			else {
				LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
				State.current.put(line[1], map);
				State.current = map;
			}
			
			State.tree_node_stack.push(State.current);
			
		});
		handler.put("F", line -> {
			// pop tree_node (node in field)
			
			State.tree_node_stack.pop();
			State.current = State.tree_node_stack.peek();
			
		});
		handler.put("G", line -> {
			// push tree_node, step 1 (node not in field)
			//
			// 4 - contains node elsewhere but not in field
			// HANDLE_TREENODELAYOUT4(NAME, VALUE)
			// printf("G %s {\n", #NAME)
			
			if(line[1].contains("[")) {
				LinkedList<LinkedHashMap<String, Object>> list = new LinkedList<LinkedHashMap<String, Object>>();
				if(State.current.containsKey(line[1]))
					list = (LinkedList<LinkedHashMap<String, Object>>) State.current.get(line[1]);
				else
					State.current.put(line[1], list);
				State.current = new LinkedHashMap<String, Object>();
				list.add(State.current);
			}
			else {
				LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
				State.current.put(line[1], map);
				State.current = map;
			}
			
			State.tree_node_stack.push(State.current);
			
		});
		handler.put("H", line -> {
			// pop tree_node (node not in field)
			
			State.tree_node_stack.pop();
			State.current = State.tree_node_stack.peek();
			
		});
		handler.put("I", line -> {
			// set property (was in field)
			//
			// 5 - contains property directly in field
			// HANDLE_TREENODELAYOUT5(TYPE, FORMAT, FIELD)
			// printf("I %s.%s [%s]: " FORMAT "\n", #TYPE, #FIELD, FORMAT, TNL_FIELD(TYPE, FIELD))
			
			if(line[1].contains("[")) {
				LinkedList<String> list = new LinkedList<String>();
				if(State.current.containsKey(line[1]))
					list = (LinkedList<String>) ((LinkedList<Object>) State.current.get(line[1])).get(0);
				else
					State.current.put(line[1], new LinkedList<Object>(java.util.Arrays.asList(list, line[2].substring(1, line[2].length() - 2))));
				list.add(line[3]);
			}
			else {
				State.current.put(line[1], new LinkedList<String>(java.util.Arrays.asList(line[3], line[2].substring(1, line[2].length() - 2))));
			}
			
		});
		handler.put("J", line -> {
			// set property (was not in field)
			//
			// 6 - contains property indirectly or not in field
			// HANDLE_TREENODELAYOUT6(NAME, FORMAT, VALUE)
			// printf("J %s [%s]: " FORMAT "\n", #NAME, FORMAT, VALUE)
			//
			// 7 - contains property at offset
			// HANDLE_TREENODELAYOUT7(TYPE, FORMAT, SIZE, OFFSET, FIELD)
			// HANDLE_TREENODELAYOUT6(TYPE.FIELD, FORMAT, FIELD)
			
			if(line[1].contains("[")) {
				LinkedList<String> list = new LinkedList<String>();
				if(State.current.containsKey(line[1]))
					list = (LinkedList<String>) ((LinkedList<Object>) State.current.get(line[1])).get(0);
				else
					State.current.put(line[1], new LinkedList<Object>(java.util.Arrays.asList(list, line[2].substring(1, line[2].length() - 2))));
				list.add(line[3]);
			}
			else {
				State.current.put(line[1], new LinkedList<String>(java.util.Arrays.asList(line[3], line[2].substring(1, line[2].length() - 2))));
			}
			
		});
		handler.put("X", line -> {
			// pass
			
		});
		handler.put("Y", line -> {
			// pass
			
		});
		handler.put("Z", line -> {
			// unhandled treecode
			
			System.err.println("unhandled treecode: " + java.util.Arrays.asList(line));
			
		});
		handler.put("", line -> {
			// unhandled record type
			
			System.err.println("unhandled record type: " + java.util.Arrays.asList(line));
			
		});
		
		
		new java.io.BufferedReader(
			new java.io.InputStreamReader(System.in)
		)
		.lines()
		.map(line -> line.split(" +"))
		.forEach(line ->
			
			
			handler.get(handler.containsKey(line[0]) ? line[0] : "").accept(line)
		);
		
		
		new java.io.ObjectOutputStream(System.out).writeObject(new Object[] {State.symbol_tables, refs});
		
		
		System.err.println("structurize exit");
	}
}

