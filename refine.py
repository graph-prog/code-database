

# export SERIALIZATION_PROTOCOL=1; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | python3 structurize.py | python3 refine.py | python3 pretty2.py


import os
import pickle
import sys
from lib.serialize import *


refs1 = {}
refs2 = set()


def start():
	while True:
		
		symbol_table = serialize_read()
		if not symbol_table:
			break
		
		traverse_tree1(symbol_table) # filter and reduce I
		
		symbol_table = symbol_table["symbol_table.$nodes"]
		traverse_tree3(symbol_table) # reduce II
		
		traverse_tree2(symbol_table) # recover filtered ref targets
		
		serialize_write(symbol_table)


def traverse_tree1(x): # filter and reduce
	
	if isinstance(x, list):
		for y in x:
			traverse_tree1(y)
	
	elif isinstance(x, dict):
		for y in tuple(x.keys()):
			
			traverse_tree1(x[y])
			
			if y == ".addr":
				refs1[x[y][0]] = x
				x[y] = x[y][0]
			
			elif y not in fields_filter:
				del x[y]
			
			elif y in fields_reduce_str:
				x[y] = reduce_str(x[y])
			
			elif isinstance(x[y], list) and len(x[y]) == 2 and x[y][1] == "%s":
				x[y] = x[y][0]
			
			elif isinstance(x[y], list) and len(x[y]) == 2 and x[y][1] == "%x":
				x[y] = int(x[y][0], 16)
			
			elif isinstance(x[y], dict) and not len(x[y]):
					x[y] = None
	
	elif isinstance(x, str):
		pass
	
	else:
		raise NotImplementedError


def reduce_str(x):
	return "".join(
		map(chr,
			map(int,
				x[0]
			)
		)
	)


def traverse_tree2(x): #recover filtered ref targets
	
	if isinstance(x, list):
		for y in x:
			traverse_tree2(y)
	
	elif isinstance(x, dict):
		for y in tuple(x.keys()):
			
			traverse_tree2(x[y])
			
			if y == ".addr":
				refs2.add(x[y])
			
			elif y == ".ref":
				key = x[y]
				if key in refs2:
					x[".ref_ok"] = True
				else:
					refs2.add(key)
					del x[y]
					x.update(refs1[key])
					break


def traverse_tree3(x): # reduce II
	
	if isinstance(x, list):
		for y in x:
			traverse_tree3(y)
	
	elif isinstance(x, dict):
		
		if "symtab_node.$decl" in x:
			
			body = x["symtab_node.$decl"]["tree_function_decl.$saved_tree"]["tree_exp.$operands[i]"]
			body = tuple(y for y in body if ".selector" in y and y[".selector"] == "STATEMENT_LIST")[0]
			#body = body["tree_statement_list.$nodes[i]"]
			#body = tuple(y for y in body if ".selector" in y and y[".selector"] == "STATEMENT_LIST")[0]
			
			#body = tuple(y for y in body if ".selector" not in y)
			
			y = {
				"type": "function",
				"file": x["symtab_node.$decl"]["tree_decl_minimal.$context"]["tree_decl_minimal.$name"]["tree_identifier.id.str[i]"],
				"name": x["symtab_node.name()[i]"],
				"params": "Not Implemented Yet",
				"body": body
			}
			x.clear();
			x.update(y);
		
		for y in tuple(x.keys()):
			traverse_tree3(x[y])


fields_filter = {
	".addr",
	#".len",
	".selector",
	#".tcc",
	#".tnl",
	".ref",
	"symbol_table.$nodes",
	#"symbol_table.cgraph_count",
	#"symbol_table.cgraph_max_summary_id",
	#"symbol_table.cgraph_max_uid",
	#"symbol_table.cpp_implicit_aliases_done",
	#"symbol_table.edges_count",
	#"symbol_table.edges_max_summary_id",
	#"symbol_table.edges_max_uid",
	#"symbol_table.function_flags_ready",
	#"symbol_table.global_info_ready",
	#"symbol_table.max_unit",
	#"symbol_table.order",
	#"symbol_table.state",
	#"symbol_table.symbol_suffix_separator",
	"symtab_node.$decl",
	#"symtab_node.asm_name()[i]",
	#"symtab_node.dump_asm_name()[i]",
	#"symtab_node.dump_name()[i]",
	#"symtab_node.flags",
	#"symtab_node.get_symtab_type_string()[i]",
	#"symtab_node.get_visibility_string()[i]",
	"symtab_node.name()[i]",
	#"symtab_node.order",
	#"symtab_node.resolution",
	#"symtab_node.type",
	#"tree_base.code",
	#"tree_base.flags",
	#"tree_base.special",
	#"tree_block.$abstract_origin",
	#"tree_block.$chain",
	#"tree_block.$fragment_chain",
	#"tree_block.$fragment_origin",
	#"tree_block.$nonlocalized_vars[i]",
	#"tree_block.$subblocks",
	#"tree_block.$supercontext",
	"tree_block.$vars",
	#"tree_block.block_num",
	#"tree_block.die",
	#"tree_block.end_locus",
	#"tree_block.locus",
	#"tree_complex.$imag",
	#"tree_complex.$real",
	#"tree_constructor.elts[i].$index",
	#"tree_constructor.elts[i].$value",
	#"tree_decl_common.$abstract_origin",
	#"tree_decl_common.$attributes",
	"tree_decl_common.$initial",
	#"tree_decl_common.$size",
	#"tree_decl_common.$size_unit",
	#"tree_decl_common.flags",
	#"tree_decl_common.pt_uid",
	"tree_decl_minimal.$context",
	"tree_decl_minimal.$name",
	#"tree_decl_minimal.locus",
	#"tree_decl_minimal.uid",
	#"tree_decl_with_vis.$assembler_name",
	#"tree_decl_with_vis.flags",
	"tree_exp.$operands[i]",
	#"tree_exp.locus",
	#"tree_field_decl.$bit_field_type",
	#"tree_field_decl.$bit_offset",
	#"tree_field_decl.$fcontext",
	#"tree_field_decl.$offset",
	#"tree_field_decl.$qualifier",
	#"tree_fixed_cst.fixed_cst_ptr",
	"tree_function_decl.$arguments",
	#"tree_function_decl.$function_specific_optimization",
	#"tree_function_decl.$function_specific_target",
	#"tree_function_decl.$personality",
	"tree_function_decl.$saved_tree",
	#"tree_function_decl.$vindex",
	#"tree_function_decl.flags",
	#"tree_function_decl.function_code",
	#"tree_identifier.id.hash_value",
	#"tree_identifier.id.len",
	"tree_identifier.id.str[i]",
	#"tree_int_cst.val[i]",
	#"tree_label_decl.eh_landing_pad_nr",
	#"tree_label_decl.label_decl_uid",
	#"tree_list.$purpose",
	#"tree_list.$value",
	#"tree_omp_clause.$ops[i]",
	#"tree_omp_clause.locus",
	#"tree_poly_int_cst.$coeffs[i]",
	#"tree_real_cst.real_cst_ptr",
	#"tree_ssa_name.$var",
	"tree_statement_list.$nodes[i]",
	#"tree_string.length",
	#"tree_string.str[i]",
	#"tree_translation_unit_decl.language[i]",
	#"tree_type_common.$attributes",
	#"tree_type_common.$canonical",
	#"tree_type_common.$context",
	#"tree_type_common.$main_variant",
	#"tree_type_common.$name",
	#"tree_type_common.$next_variant",
	#"tree_type_common.$pointer_to",
	#"tree_type_common.$reference_to",
	#"tree_type_common.$size",
	#"tree_type_common.$size_unit",
	#"tree_type_common.alias_set",
	#"tree_type_common.flags",
	#"tree_type_common.symtab.die",
	#"tree_type_common.uid",
	#"tree_typed.$type",
	#"tree_vec.$a[i]",
	#"tree_vector.$elts[i]",
}

fields_reduce_str = {
	"symtab_node.asm_name()[i]",
	"symtab_node.dump_asm_name()[i]",
	"symtab_node.dump_name()[i]",
	"symtab_node.get_symtab_type_string()[i]",
	"symtab_node.get_visibility_string()[i]",
	"symtab_node.name()[i]",
	"tree_identifier.id.str[i]",
	"tree_string.str[i]",
	"tree_translation_unit_decl.language[i]",
}


start()

