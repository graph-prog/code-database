

// rustc -o structurize_rs structurize.rs && rustc -o pretty2_rs pretty2.rs && rustc -o refine_rs refine.rs
// rustc -O -o structurize_rs structurize.rs && rustc -O -o pretty2_rs pretty2.rs && rustc -O -o refine_rs refine.rs
//
// export SERIALIZATION_PROTOCOL=2; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | ./structurize_rs | ./pretty2_rs


#![allow(unused_imports)]

use std::any::Any;
use std::boxed::Box;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::io::{BufReader, BufWriter, Lines, Read, Stdin, StdinLock, Stdout, Write};
use std::iter::Peekable;
use std::vec::Vec;

mod lib {
	pub mod serialize;
}

use lib::serialize::LinkedHashMap;


fn main() {
	
	
	let input = std::io::stdin();
	let mut input = BufReader::new(input);
	
	
	loop {
		
		
		let symbol_tables = lib::serialize::serialize_read(&mut input);
		if symbol_tables.is::<Option<()>>() {
			break;
		}
		
		
		lib::serialize::serialize_pretty(&symbol_tables);
	}
}

