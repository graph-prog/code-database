

# STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | bash pretty1.bash


ind=0


cat | while read x; do
	
	[[ "${x: -1}" == '}' ]] && ind=$((ind-1))
	
	yes $'\t' | head -n "$ind" | tr -d '\n'
	echo "$x"
	
	[[ "${x: -1}" == '{' ]] && ind=$((ind+1))
done

