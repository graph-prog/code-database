

// javac structurize.java -Xlint && javac pretty2.java -Xlint
// export SERIALIZATION_PROTOCOL=1; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | java -Xms8g -Xmx8g -Xss1g Structurize | java -Xms8g -Xmx8g -Xss1g Pretty


import java.util.LinkedList;
import java.util.LinkedHashMap;
import java.util.Stack;


class Pretty {
	
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws java.io.IOException, ClassNotFoundException {
		
		
		Object[] in = (Object[]) new java.io.ObjectInputStream(System.in).readObject();
		LinkedList<LinkedHashMap<String, Object>> symbol_tables = (LinkedList<LinkedHashMap<String, Object>>) in[0];
		LinkedHashMap<String, LinkedHashMap<String, Object>> refs = (LinkedHashMap<String, LinkedHashMap<String, Object>>) in[1];
		
		
		/*for(LinkedHashMap<String, Object> node : refs.values())
			/*node.entrySet().stream()
			.map(prop -> (prop.getValue() instanceof Object[]) ? java.util.Map.entry(prop.getKey(), "!!!") : prop)
			.map(prop -> (prop.getValue() instanceof String[]) ? java.util.Map.entry(prop.getKey(), "!!!") : prop)
			.collect(java.util.stream.Collectors.toMap(java.util.Map.Entry::getKey, java.util.Map.Entry::getValue));* /
			for(java.util.Map.Entry<String, Object> prop : new LinkedList<java.util.Map.Entry<String, Object>>(node.entrySet()))
				if(prop.getValue() instanceof Object[]
				|| prop.getValue() instanceof String[])
					node.put(prop.getKey(), "234234" + java.util.Arrays.asList(prop.getValue()));*/
		
		
		String lines = "" + symbol_tables;
		
		lines = lines.replace("()[i]", "[i]");
		lines = lines.replace("=", ": ");
		lines = lines.replaceAll("([-\\w\\.%$]|\\[i\\])+", "'$0'");
		
		lines = lines.replace("\n", "");
		lines = String.join("\n", lines.split("(?<=\\[)"));
		lines = String.join("\n", lines.split("(?=\\])"));
		lines = String.join("\n", lines.split("(?<=\\()"));
		lines = String.join("\n", lines.split("(?=\\))"));
		lines = String.join("\n", lines.split("(?<=\\{)"));
		lines = String.join("\n", lines.split("(?=\\})"));
		lines = String.join("\n", lines.split("(?<=,)"));
		lines = lines.replace("\n\n", "\n");
		lines = lines.replace("[\ni\n]", "[i]");
		
		
		int ind = 0;
		for(String line : lines.split("\n")) {
			
			if(!line.isEmpty() && (new java.util.HashSet<String>(java.util.Arrays.asList("]", ")", "}")).contains(line.substring(0, 1)))) ind -= 1;
			System.out.println("\t".repeat(ind) + line.trim());
			if(!line.isEmpty() && (new java.util.HashSet<String>(java.util.Arrays.asList("[", "(", "{")).contains(line.substring(line.length() - 1)))) ind += 1;
		}
	}
}


/*


*/

