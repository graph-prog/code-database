

# export SERIALIZATION_PROTOCOL=1; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | python3 structurize.py | hd


import os
import pickle
import sys
from lib.serialize import *


symbol_table = None
symtab_nodes = None
tree_node_stack = None
current = None

#refs = {}


for line in sys.stdin:
	line = line.split()
	
	
	if line[0] == "A":
		# push symbol_table
		
		current = symbol_table = {}
		symtab_nodes = None
		tree_node_stack = [current]
	
	
	elif line[0] == "B":
		# push symtab_node
		
		if not symtab_nodes: # append to end
			symtab_nodes = []
			current["symbol_table.$nodes"] = symtab_nodes
		
		symtab_nodes.append({})
		current = symtab_nodes[-1]
		tree_node_stack = [current]
	
	
	elif line[0] == "C":
		# push tree_node, step 2
		#
		# printf("C SELECTOR %d %s  LEN %d  TCC %d %s  TNL %d %s  (%016llx)\n", SELECTOR, _SELECTOR, LEN, TCC, _TCC, TNL, _TNL, t)
		
		current = tree_node_stack[-1]
		
		current[".selector"] = [line[3],"%s"]
		current[".len"] = [line[5],"%d"]
		current[".tcc"] = [line[8],"%s"]
		current[".tnl"] = [line[11],"%s"]
		current[".addr"] = [line[12][1:-1],"%x"]
		
		#refs[current[".addr"][0]] = current
	
	
	elif line[0] == "D":
		# push tree_node, step 2 (reference to other tree_node)
		#
		# printf("D REF  (%016llx)\n", t)
		
		#current.update(refs[line[2][1:-1]])
		current[".ref"] = line[2][1:-1] # TODO activate when ready
	
	
	elif line[0] == "E":
		# push tree_node, step 1 (node in field)
		#
		# 3 - contains node in field
		# HANDLE_TREENODELAYOUT3(TYPE, FIELD)
		# printf("E %s.%s {\n", #TYPE, #FIELD)
		
		if "[" in line[1]:
			current[line[1]] = current.get(line[1]) or []
			current[line[1]].append({})
			current = current[line[1]][-1]
		else:
			current[line[1]] = {}
			current = current[line[1]]
		
		tree_node_stack.append(current)
	
	
	elif line[0] == "F":
		# pop tree_node (node in field)
		
		tree_node_stack.pop()
		current = tree_node_stack[-1]
	
	
	elif line[0] == "G":
		# push tree_node, step 1 (node not in field)
		#
		# 4 - contains node elsewhere but not in field
		# HANDLE_TREENODELAYOUT4(NAME, VALUE)
		# printf("G %s {\n", #NAME)
		
		if "[" in line[1]:
			current[line[1]] = current.get(line[1]) or []
			current[line[1]].append({})
			current = current[line[1]][-1]
		else:
			current[line[1]] = {}
			current = current[line[1]]
		
		tree_node_stack.append(current)
	
	
	elif line[0] == "H":
		# pop tree_node (node not in field)
		
		tree_node_stack.pop()
		current = tree_node_stack[-1]
	
	
	elif line[0] == "I":
		# set property (was in field)
		#
		# 5 - contains property directly in field
		# HANDLE_TREENODELAYOUT5(TYPE, FORMAT, FIELD)
		# printf("I %s.%s [%s]: " FORMAT "\n", #TYPE, #FIELD, FORMAT, TNL_FIELD(TYPE, FIELD))
		
		if "[" in line[1]:
			current[line[1]] = current.get(line[1]) or [[],line[2][1:-2]]
			current[line[1]][0].append(line[3])
		else:
			current[line[1]] = [line[3],line[2][1:-2]]
	
	
	elif line[0] == "J":
		# set property (was not in field)
		#
		# 6 - contains property indirectly or not in field
		# HANDLE_TREENODELAYOUT6(NAME, FORMAT, VALUE)
		# printf("J %s [%s]: " FORMAT "\n", #NAME, FORMAT, VALUE)
		#
		# 7 - contains property at offset
		# HANDLE_TREENODELAYOUT7(TYPE, FORMAT, SIZE, OFFSET, FIELD)
		# HANDLE_TREENODELAYOUT6(TYPE.FIELD, FORMAT, FIELD)
		
		if "[" in line[1]:
			current[line[1]] = current.get(line[1]) or [[],line[2][1:-2]]
			current[line[1]][0].append(line[3])
		else:
			current[line[1]] = [line[3],line[2][1:-2]]
	
	
	elif line[0] == "X":
		# pop symtab_node
		
		pass
	
	
	elif line[0] == "Y":
		# pop symbol_table
		
		serialize_write(symbol_table)
	
	
	elif line[0] == "Z":
		# unhandled treecode
		
		print("unhandled treecode: ", line, file=sys.stderr)
	
	
	else:
		# unhandled record type
		
		print("unhandled record type: ", line, file=sys.stderr)


print("structurize exit", file=sys.stderr)

