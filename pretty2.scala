

// export SERIALIZATION_PROTOCOL=1; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | scala -savecompiled -nc -J-Xms8g -J-Xmx8g -J-Xss1g structurize.scala | scala -savecompiled -nc -J-Xms8g -J-Xmx8g -J-Xss1g pretty2.scala


import scala.collection.mutable.{ListBuffer => List}
import scala.collection.mutable.{LinkedHashMap => Map}
import scala.collection.mutable.Stack


var (symbol_tables, refs) = new java.io.ObjectInputStream(System.in).readObject.asInstanceOf[(List[Map[String, Any]], Map[String, Map[String, Any]])]


var lines = "" + symbol_tables

lines = lines.replace("()[i]", "[i]")
lines = lines.replace("ListBuffer(", "[")
lines = lines.replace("Map(", "[")
lines = lines.replace("(", "[")
lines = lines.replace(")", "]")
lines = lines.replace(" -> ", ": ")
lines = lines.replaceAll("([-\\w\\.%$]|\\[i\\])+", "'$0'")

lines = lines.replace("\n", "")
lines = lines.split("(?<=\\[)").mkString("\n")
lines = lines.split("(?=\\])").mkString("\n")
lines = lines.split("(?<=\\()").mkString("\n")
lines = lines.split("(?=\\))").mkString("\n")
lines = lines.split("(?<=\\{)").mkString("\n")
lines = lines.split("(?=\\})").mkString("\n")
lines = lines.split("(?<=,)").mkString("\n")
lines = lines.replace("\n\n", "\n")
lines = lines.replace("[\ni\n]", "[i]")


var ind = 0
for(line <- lines.split("\n")) {
	
	if(!line.isEmpty && (Set("]", ")", "}") contains line.substring(0, 1))) ind -= 1
	println("\t" * ind + line.trim)
	if(!line.isEmpty && (Set("[", "(", "{") contains line.substring(line.length - 1))) ind += 1
}

