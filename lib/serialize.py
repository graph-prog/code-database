

import os
import pickle
import sys


def serialize_write(x):
	SERIALIZATION_PROTOCOL = os.environ.get("SERIALIZATION_PROTOCOL")
	
	if SERIALIZATION_PROTOCOL == "1":
		
		sys.setrecursionlimit(2**31-1)
		pickle.dump(x,sys.stdout.buffer)
		
	elif SERIALIZATION_PROTOCOL == "2":
		
		def write_node(x):
			if isinstance(x, list):
				sys.stdout.buffer.write(b"L")
				sys.stdout.buffer.write(len(x).to_bytes(4, "little"))
				for y in x:
					write_node(y)
			elif isinstance(x, dict):
				sys.stdout.buffer.write(b"M")
				sys.stdout.buffer.write(len(x).to_bytes(4, "little"))
				for y in x.keys():
					write_node(y)
				for y in x.keys():
					write_node(x[y])
			elif isinstance(x, str):
				sys.stdout.buffer.write(b"S")
				y = x.encode()
				sys.stdout.buffer.write(len(y).to_bytes(4, "little"))
				sys.stdout.buffer.write(y)
			else:
				raise NotImplementedError
		
		sys.setrecursionlimit(2**31-1)
		write_node(x)
		
	elif SERIALIZATION_PROTOCOL == "3":
		raise NotImplementedError
	else:
		raise NotImplementedError


def serialize_read():
	SERIALIZATION_PROTOCOL = os.environ.get("SERIALIZATION_PROTOCOL")
	
	if SERIALIZATION_PROTOCOL == "1":
		
		try:
			sys.setrecursionlimit(2**31-1)
			return pickle.load(sys.stdin.buffer)
		except EOFError:
			return None
		except pickle.UnpicklingError:
			return None
		
	elif SERIALIZATION_PROTOCOL == "2":
		
		def read_node():
			z = sys.stdin.buffer.read(1)
			if z == b"":
				return None
			elif z == b"L":
				len = sys.stdin.buffer.read(4)
				len = int.from_bytes(len, "little")
				xs = []
				for i in range(len):
					xs.append(read_node())
				return xs
			elif z == b"M":
				len = sys.stdin.buffer.read(4)
				len = int.from_bytes(len, "little")
				ys = []
				xs = {}
				for i in range(len):
					ys.append(read_node())
				for y in ys:
					xs[y] = read_node()
				return xs
			elif z == b"S":
				len = sys.stdin.buffer.read(4)
				len = int.from_bytes(len, "little")
				x = sys.stdin.buffer.read(len)
				x = x.decode()
				return x
			else:
				raise NotImplementedError
		
		sys.setrecursionlimit(2**31-1)
		return read_node()
		
	elif SERIALIZATION_PROTOCOL == "3":
		raise NotImplementedError
	else:
		raise NotImplementedError


def serialize_pretty(x):
	
	sys.setrecursionlimit(2**31-1)
	lines = str(x)
	lines = lines.replace("\n","")
	lines = lines.replace("()[i]","[i]")
	lines = "[\n".join(lines.split("["))
	lines = "\n]".join(lines.split("]"))
	lines = "(\n".join(lines.split("("))
	lines = "\n)".join(lines.split(")"))
	lines = "{\n".join(lines.split("{"))
	lines = "\n}".join(lines.split("}"))
	lines = ",\n".join(lines.split(","))
	lines = lines.replace("\n\n","\n")
	lines = lines.replace("[\ni\n]","[i]")
	lines = lines.split("\n")
	
	ind = 0
	for line in lines:
		
		if line[0] in {"]",")","}"}: ind -= 1
		print("\t" * ind + line.lstrip())
		if line[-1] in {"[","(","{"}: ind += 1

