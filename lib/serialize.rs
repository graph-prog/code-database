

#![allow(unused_imports)]

use std::any::Any;
use std::boxed::Box;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::io::{BufReader, BufWriter, Lines, Read, Stdin, StdinLock, Stdout, Write};
use std::iter::Peekable;
use std::vec::Vec;

pub struct LinkedHashMap {
	pub vec: Vec::<String>,
	pub map: HashMap::<String, Box<dyn Any>>,
}

#[allow(dead_code)]
impl LinkedHashMap {
	
	pub fn new(x: usize) -> Self {
		Self {
			vec: Vec::with_capacity(x),
			map: HashMap::with_capacity(x),
		}
	}
	
	pub fn insert(&mut self, key: String, val: Box<dyn Any>) {
		self.vec.push(key.clone());
		self.map.insert(key, val);
	}
}


#[allow(dead_code)]
pub fn serialize_write(x: &Box<dyn Any>) {
	
	match std::env::var("SERIALIZATION_PROTOCOL").unwrap_or(String::new()).as_str() {
		
		"1" => unimplemented!(),
		
		"2" => {
			
			let output = std::io::stdout();
			let mut output = BufWriter::new(output);
			
			fn write_node(x: &Box<dyn Any>, output: &mut BufWriter<Stdout>) {
				
				if x.is::<Vec::<Box<dyn Any>>>() {
					let x = x.downcast_ref::<Vec::<Box<dyn Any>>>().unwrap();
					
					let _ = output.write(b"L");
					assert!(x.len() <= u32::MAX as usize);
					let _ = output.write(&(x.len() as u32).to_ne_bytes());
					
					for y in x.iter() {
						write_node(&y, output);
					}
				}
				else if x.is::<LinkedHashMap>() {
					let x = x.downcast_ref::<LinkedHashMap>().unwrap();
					
					let _ = output.write(b"M");
					assert!(x.vec.len() <= u32::MAX as usize);
					let _ = output.write(&(x.vec.len() as u32).to_ne_bytes());
					
					for y in x.vec.iter() {
						let y: Box<dyn Any> = Box::new(y.clone());
						write_node(&y, output);
					}
					
					for y in x.vec.iter() {
						write_node(&x.map[y], output);
					}
				}
				else if x.is::<String>() {
					let x = x.downcast_ref::<String>().unwrap();
					let x = x.as_bytes();
					assert!(x.len() <= u32::MAX as usize);
					
					let _ = output.write(b"S");
					let _ = output.write(&(x.len() as u32).to_ne_bytes());
					let _ = output.write(x);
				}
				else {
					unimplemented!();
				}
			}
			write_node(x, &mut output);
			
		},
		"3" => unimplemented!(),
		
		 _  => unimplemented!(),
	}
}


#[allow(dead_code)]
pub fn serialize_read(input: &mut BufReader<Stdin>) -> Box<dyn Any> {
	
	match std::env::var("SERIALIZATION_PROTOCOL").unwrap_or(String::new()).as_str() {
		
		"1" => unimplemented!(),
		
		"2" => {
			
			fn read_node(input: &mut BufReader<Stdin>) -> Box<dyn Any> {
				let mut buffer = Vec::<u8>::with_capacity(4);
				
				match input.by_ref().take(1).read_to_end(&mut buffer) {
					Ok(0) => return Box::<Option<()>>::new(None),
					 _  => {},
				}
				
				match &buffer[..] {
					b"L" => {
						buffer.clear();
						let _ = input.by_ref().take(4).read_to_end(&mut buffer);
						
						let x = buffer.as_slice();
						let x = <[u8; 4]>::try_from(x).unwrap();
						let x = u32::from_ne_bytes(x);
						let x = x as usize;
						
						let mut x = Vec::<Box<dyn Any>>::with_capacity(x);
						
						for _ in 0..x.capacity() {
							x.push(read_node(input));
						}
						
						return Box::new(x);
					},
					b"M" => {
						buffer.clear();
						let _ = input.by_ref().take(4).read_to_end(&mut buffer);
						
						let x = buffer.as_slice();
						let x = <[u8; 4]>::try_from(x).unwrap();
						let x = u32::from_ne_bytes(x);
						let x = x as usize;
						
						let mut x = LinkedHashMap::new(x);
						
						for _ in 0..x.vec.capacity() {
							let y = read_node(input);
							let y = y.downcast::<String>().unwrap();
							x.vec.push(*y);
						}
						
						for i in 0..x.vec.capacity() {
							x.map.insert(x.vec[i].clone(), read_node(input));
						}
						
						return Box::new(x);
					},
					b"S" => {
						buffer.clear();
						let _ = input.by_ref().take(4).read_to_end(&mut buffer);
						
						let x = buffer.as_slice();
						let x = <[u8; 4]>::try_from(x).unwrap();
						let x = u32::from_ne_bytes(x);
						let x = x as u64;
						
						buffer.clear();
						let _ = input.by_ref().take(x).read_to_end(&mut buffer);
						
						let buffer = String::from_utf8(buffer).unwrap();
						return Box::new(buffer);
					},
					 _  => unimplemented!(),
				}
			}
			return read_node(input);
			
		},
		"3" => unimplemented!(),
		
		 _  => unimplemented!(),
	}
}


#[allow(dead_code)]
pub fn serialize_pretty(x: &Box<dyn Any>) {
	
	let output = std::io::stdout();
	let mut output = BufWriter::new(output);
	
	fn pretty_node(x: &Box<dyn Any>, ind: usize, output: &mut BufWriter<Stdout>) {
		
		if x.is::<Vec::<Box<dyn Any>>>() {
			let x = x.downcast_ref::<Vec::<Box<dyn Any>>>().unwrap();
			
			let _ = output.write(b"[\n");
			
			for y in x.iter() {
				for _ in 0..ind + 1 { let _ = output.write(b"\t"); }
				pretty_node(&y, ind + 1, output);
			}
			
			for _ in 0..ind { let _ = output.write(b"\t"); }
			let _ = output.write(b"]\n");
		}
		else if x.is::<LinkedHashMap>() {
			let x = x.downcast_ref::<LinkedHashMap>().unwrap();
			
			let _ = output.write(b"{\n");
			
			for y in x.vec.iter() {
				for _ in 0..ind + 1 { let _ = output.write(b"\t"); }
				let _ = output.write(b"'");
				let _ = output.write(y.as_bytes());
				let _ = output.write(b"': ");
				pretty_node(&x.map[y], ind + 1, output);
			}
			
			for _ in 0..ind { let _ = output.write(b"\t"); }
			let _ = output.write(b"}\n");
		}
		else if x.is::<String>() {
			let x = x.downcast_ref::<String>().unwrap();
			
			let _ = output.write(b"'");
			let _ = output.write(x.as_bytes());
			let _ = output.write(b"'\n");
		}
		else {
			unimplemented!();
		}
	}
	pretty_node(x, 0, &mut output);
}

