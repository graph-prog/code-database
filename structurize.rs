

// rustc -o structurize_rs structurize.rs && rustc -o pretty2_rs pretty2.rs && rustc -o refine_rs refine.rs
// rustc -O -o structurize_rs structurize.rs && rustc -O -o pretty2_rs pretty2.rs && rustc -O -o refine_rs refine.rs
//
// export SERIALIZATION_PROTOCOL=2; STORE_GRAPH_TO_DISK=PRINT ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1 >/dev/null 2>/dev/null | ./structurize_rs | hd


#![allow(unused_imports)]

use std::any::Any;
use std::boxed::Box;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::io::{BufReader, BufWriter, Lines, Read, Stdin, StdinLock, Stdout, Write};
use std::iter::Peekable;
use std::vec::Vec;

mod lib {
	pub mod serialize;
}

use lib::serialize::LinkedHashMap;


fn main() {
	
	
	//let mut symbol_tables = Vec::<Box<dyn Any>>::new();
	
	
	let input = std::io::stdin();
	let input = input.lines();
	let mut input = input.peekable();
	loop {
		match structurize(0, b'A', &mut input) { // push symbol_table
			//Some(symbol_table) => symbol_tables.push(symbol_table),
			Some(symbol_table) => lib::serialize::serialize_write(&symbol_table),
			None => break,
		}
	}
	
	
	fn structurize(
		
		level: i32,
		mode: u8,
		input: &mut Peekable<Lines<StdinLock>>,
		
	) -> Option<Box<dyn Any>> {
		//eprintln!("structurize({})", level);
		
		
		let mut current = LinkedHashMap::new(0);
		let mut symtab_nodes = Vec::<Box<dyn Any>>::new();
		
		
		{
			let line = match input.peek() {
				Some(Ok(line)) => line,
				 _  => return None,
			};
			let line: Vec<&str> = line.split_whitespace().collect();
			
			
			match (mode, line[0]) {
				
				(b'A', "A") | (b'B', "B") => {
					
					let _ = input.next();
					
				}
				(b' ', _)  => { }
				_  => { unimplemented!(); }
			}
		}
		
		
		'read: loop {
			let line = match input.peek() {
				Some(Ok(line)) => line,
				 _  => break 'read,
			};
			let line: Vec<&str> = line.split_whitespace().collect();
			
			
			match line[0] {
				
				"A" => {
					match mode {
						
						b'A' | b'B' => {
							// pop symbol_table/symtab_node
							
							break 'read; // pop
							
						}
						 _  => { unimplemented!(); }
					}
					
				}
				"B" => {
					match mode {
						
						b'A' => {
							// push symtab_node
							
							let symtab_node = structurize(level + 1, b'B', input).unwrap();
							symtab_nodes.push(symtab_node);
							
							continue 'read; //
							
						}
						b'B' => {
							// pop symtab_node
							
							break 'read; // pop
							
						}
						 _  => { unimplemented!(); }
					}
					
				}
				"C" => {
					// push tree_node, step 2
					//
					// printf("C SELECTOR %d %s  LEN %d  TCC %d %s  TNL %d %s  (%016llx)\n", SELECTOR, _SELECTOR, LEN, TCC, _TCC, TNL, _TNL, t)
					
					{
						let mut vec = Vec::<Box<dyn Any>>::with_capacity(2);
						
						let x = line[3].to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
					
						let x = "%s".to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
						
						current.insert(".selector".to_string(), Box::new(vec));
					}
					
					{
						let mut vec = Vec::<Box<dyn Any>>::with_capacity(2);
						
						let x = line[5].to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
					
						let x = "%d".to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
						
						current.insert(".len".to_string(), Box::new(vec));
					}
					
					{
						let mut vec = Vec::<Box<dyn Any>>::with_capacity(2);
						
						let x = line[8].to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
					
						let x = "%s".to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
						
						current.insert(".tcc".to_string(), Box::new(vec));
					}
					
					{
						let mut vec = Vec::<Box<dyn Any>>::with_capacity(2);
						
						let x = line[11].to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
					
						let x = "%s".to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
						
						current.insert(".tnl".to_string(), Box::new(vec));
					}
					
					{
						let mut vec = Vec::<Box<dyn Any>>::with_capacity(2);
						
						let x = line[12][1..line[12].len() - 1].to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
					
						let x = "%x".to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec.push(x);
						
						current.insert(".addr".to_string(), Box::new(vec));
					}
					
				}
				"D" => {
					// push tree_node, step 2 (reference to other tree_node)
					//
					// printf("D REF  (%016llx)\n", t)
					
					let x = line[2][1..line[2].len() - 1].to_string();
					current.insert(".ref".to_string(), Box::new(x));
					
				}
				"E" | "G" => {
					// push tree_node, step 1 (node in field)
					//
					// 3 - contains node in field
					// HANDLE_TREENODELAYOUT3(TYPE, FIELD)
					// printf("E %s.%s {\n", #TYPE, #FIELD)
					
					// push tree_node, step 1 (node not in field)
					//
					// 4 - contains node elsewhere but not in field
					// HANDLE_TREENODELAYOUT4(NAME, VALUE)
					// printf("G %s {\n", #NAME)
					
					let key = line[1].to_string();
					let _ = input.next();
					
					let tree_node = structurize(level + 1, b' ', input).unwrap();
					
					if key.contains("[") {
						
						if current.map.contains_key(&key) {
							let vec = current.map.get_mut(&key).unwrap();
							let vec = vec.downcast_mut::<Vec::<Box<dyn Any>>>().unwrap();
						
							vec.push(tree_node);
						}
						else {
							let mut vec = Vec::<Box<dyn Any>>::with_capacity(1);
							
							vec.push(tree_node);
							
							current.insert(key, Box::new(vec));
						}
					}
					else {
						current.insert(key, tree_node);
					}
					
				}
				"F" | "H" => {
					// pop tree_node (node in field)
					
					// pop tree_node (node not in field)
					
					let current: Box<dyn Any> = Box::new(current);
					return Some(current);
					
				}
				"I" | "J" => {
					// set property (was in field)
					//
					// 5 - contains property directly in field
					// HANDLE_TREENODELAYOUT5(TYPE, FORMAT, FIELD)
					// printf("I %s.%s [%s]: " FORMAT "\n", #TYPE, #FIELD, FORMAT, TNL_FIELD(TYPE, FIELD))
					
					// set property (was not in field)
					//
					// 6 - contains property indirectly or not in field
					// HANDLE_TREENODELAYOUT6(NAME, FORMAT, VALUE)
					// printf("J %s [%s]: " FORMAT "\n", #NAME, FORMAT, VALUE)
					//
					// 7 - contains property at offset
					// HANDLE_TREENODELAYOUT7(TYPE, FORMAT, SIZE, OFFSET, FIELD)
					// HANDLE_TREENODELAYOUT6(TYPE.FIELD, FORMAT, FIELD)
					
					if line[1].contains("[") {
						
						if current.map.contains_key(line[1]) {
							let vec2 = current.map.get_mut(line[1]).unwrap();
							let vec2 = vec2.downcast_mut::<Vec::<Box<dyn Any>>>().unwrap();
							let vec2 = &mut vec2[0];
							let vec2 = vec2.downcast_mut::<Vec::<Box<dyn Any>>>().unwrap();
						
							let x = line[3].to_string();
							let x: Box<dyn Any> = Box::new(x);
							vec2.push(x);
						}
						else {
							let mut vec2 = Vec::<Box<dyn Any>>::with_capacity(1);
						
							let x = line[3].to_string();
							let x: Box<dyn Any> = Box::new(x);
							vec2.push(x);
							
							let mut vec1 = Vec::<Box<dyn Any>>::with_capacity(2);
							
							let x = vec2;
							let x: Box<dyn Any> = Box::new(x);
							vec1.push(x);
						
							let x = line[2][1..line[2].len() - 2].to_string();
							let x: Box<dyn Any> = Box::new(x);
							vec1.push(x);
							
							current.insert(line[1].to_string(), Box::new(vec1));
						}
					}
					else {
						let mut vec1 = Vec::<Box<dyn Any>>::with_capacity(2);
						
						let x = line[3].to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec1.push(x);
					
						let x = line[2][1..line[2].len() - 2].to_string();
						let x: Box<dyn Any> = Box::new(x);
						vec1.push(x);
						
						current.insert(line[1].to_string(), Box::new(vec1));
					}
					
				}
				"X" | "Y" => {
					// pass
					
				}
				"Z" => {
					// unhandled treecode
					
					eprintln!("unhandled treecode: {:?}", line);
					
				}
				 _  => {
					// unhandled record type
					
					eprintln!("unhandled record type: {:?}", line);
					
				}
			}
			
			let _ = input.next();
		}
		
		
		{
			match mode {
				
				b'A' => {
					if !symtab_nodes.is_empty() {
						current.insert("symbol_table.$nodes".to_string(), Box::new(symtab_nodes));
					}
					
				}
				 _  => {}
			}
		}
		
		
		let current: Box<dyn Any> = Box::new(current);
		return Some(current);
	}
	
	
	//let symbol_tables: Box<dyn Any> = Box::new(symbol_tables);
	//lib::serialize::serialize_write(&symbol_tables);
}

