# code-database

Mit der "Datenbank für Programmcode" wird der Programmcode von Software-Projekten maschinell untersuchbar und (in einem späteren Stadium) transformierbar gemacht. Außerdem können

* die Ergebnisse von Programmcode-Analysen,
* über normale Programmier-Sprachmittel hinausgehende Architektur-/Projektstrukturierungen,
* maschinell auswertbare Markierungen und
* beliebige andere Informationen gespeichert werden.

Dieses Repository informiert über den aktuellen Stand der Umsetzung.

[TOC]

## Roadmap

- [x] Patch (Modifikation) für GCC schreiben, mit dem die Bestandteile des Programmcodes ausgeleitet werden
- [X] Einige Open-Source-Projekte kompilieren und ihre Programmcode-Bestandteile in einem effizienten Format auf der Festplatte speichern
- [ ] November: Die Abfragesprache integrieren, sodass beliebige Zusammenhänge in den gesammelten Daten gesucht werden können
- [ ] Dezember: Die statische Analyse integrieren, sodass mit der Abfragesprache zusätzlich Fragen
     * zum Ablauf der Programms,
     * zum Fluss von Informationen,
     * zur An-/Abwesenheit bestimmter Merkmale und
     * zur Klassifikation der beabsichtigten Zwecke (Mustererkennung) beantwortet werden können\
&nbsp;
- [ ] Praktische Nutzbarkeit der Abfragesprache erhöhen; Nützlichkeit mit den gesammelten Daten validieren; mögliche Bereiche:
     * Definitionen auf Feature-Ebene und Kollaborations-Ebene,
     * Definitionen auf Architektur-Ebene und Plattformwahl-Ebene,
     * alltägliche Helfer auf Umsetzungs-Ebene,
     * modellgetriebene Umsetzung,
     * Eigenschaften der Umsetzung bewerten und Vorgaben machen,
     * gewünschte Eigenschaften der Umsetzung automatisch hinzufügen (qualitativ erweitern),
     * Umfang der Umsetzung automatisch extrapolieren (quantitativ erweitern)
- [ ] Demo-Projekt konstruieren (von Anforderung bis Umsetzung), überzeugende Möglichkeiten der Abfragesprache auswählen, daraus praktische Anwendungsfälle konstruieren und mittels Abfragesprache formulieren

## Erklärung

Um aus dem Programmcode eines Software-Projekts eine ausführbare Datei zu erstellen, übersetzt das Programmierwerkzeug GCC den Programmcode zunächst in eine Graph-Repräsentation (eine Art Straßenkarte oder Schaltplan). Dieser Code-Graph enthält die Zusammenhänge und die Bedeutung, die der Programmierer ausdrücken möchte und die er mit Programmier-Sprachmitteln notiert hat.

Aus der internen Graph-Repräsentation wird im zweiten Schritt eine ausführbare Datei erstellt ("kompiliert"). Wir interessieren uns jedoch nur für den zwischenzeitlich erstellten Code-Graphen, den wir 1:1 auf der Festplatte speichern.

~~~
┌──────────────┐        ┌──────────────┐        ┌──────────────┐
│   Programm-  │        │ intermediär: │        │ ausführbare  │
│     Code     │───────▶│  Code-Graph  │╌╌╌╌╌╌╌▶│ Datei (.exe) │
│       ______ │        │       ______ │        │       ______ │
 \_____/      \          \_____/      \          \_____/      \
                               │
                               │ 
                               ▼
                          ╭──────────╮
                          ╰──────────╯
                          ╰─  Fest- ─╯
                          ╰─ platte ─╯
                          ╰──────────╯
~~~

## Schrittweise Verfeinerung

~~~
                        │              │
                        │       ______ │
                         \_____/      \
                               │
                               │
                 ┌──────────────┐  Interne Graph-Repräsentation (symtab_node, tree_node)
                     ┌──────────┐  Unstrukturiertes Ergebnis der Graph-Suche in Datei ausleiten
                         ┌──────┐  Aufbereitung für die Nutzung als Datenbank
                             ┌──┐  Den Programmcode in seine "natürliche" Form bringen 
                               │
                               │
                               ▼
                          ╭──────────╮
                          ╰──────────╯
                          ╰─        ─╯
                          ╰─        ─╯
~~~

## Das Datenformat I

<details>
<summary>Sehr technisch und daher schwer verständlich (zum Öffnen anklicken)</summary>

Die Datenbank verwendet ein Datenformat ähnlich zu [FlatBuffers](https://flatbuffers.dev). Auf Festplatte und RAM wird (soweit möglich) dasselbe Datenformat verwendet, sodass keine Konvertierung notwendig ist. In der Anfangsphase des Projekts werden alle Daten als Schlüssel-Wert-Paare gespeichert; später besteht die Möglichkeit, häufig auftretende Schlüssel direkt addressierbar zu machen.

Pseudocode:

```
struct {
	uint64_t ref_addr;                      // unique id for references to this object
	uint32_t type;                          // generic or collection or standardized-struct + generic
	uint32_t len;                           // number of keys = number of vals; only for list: number of keys = 0
	uint32_t data_gen_size;                 // = off[-1]
	uint32_t off_keys[len];                 // in descending order; size[i] = off[i-1] - off[i]
	uint32_t off_vals[len-1];               // off[last = len-1] = 0
	uint8_t data_std[<according to type>];  // size = 0 for generic and collection; size > 0 for standardized-struct
	uint8_t data_gen[data_gen_size];        // values and keys; saturate access to bounds
}
```

Die Werte von Schlüssel-Wert-Paaren werden als Zeichenkette gespeichert. Die Interpretation bspw. als Ganzzahl erfolgt nachträglich bei der Verwendung der gespeicherten Inhalte.

Jedes Objekt wird anhand einer fortlaufenden Nummer identifiziert. Verweise zwischen Objekten (in der Baumstruktur enthaltene Objekte, Duplikate, zirkuläre Verweise und aufgelöste Bezeichner) werden aufgelöst durch die Zuordnung: fortlaufende Nummer -> Speicheradresse. Zur besseren Klassifizierbarkeit wird den zugehörigen Schlüsseln das Präfix "$" vorangestellt.

Kollektionen (z. B. Listen) werden über generische Schlüssel-Wert-Paare abgebildet. In der einfachen Variante geschieht der Zugriff darauf ohne Konvertierung. In der effizienten Variante findet zwischen RAM und Festplatte ausnahmsweise eine Konvertierung statt. Falls notwendig, sind so auch effizientere Schlüssel-Wert-Zuordnungen möglich.

Eine einzelne Datei enthält eine beliebige Menge von Objekten. Je nach Anwendungszweck können bspw. pro Funktion eine separate Datei verwendet oder das ganze Projekt in einer zusammengefassten Datei gespeichert werden. Das erste Objekt in jeder Datei beschreibt die Art der restlichen enthaltenen Objekte; über alle Dateien wird ein Verzeichnis erstellt, um zur gesuchten Information die richtige Datei zu finden.

Die Verarbeitung der Daten geschieht im RAM. Alle Änderungen im RAM werden (transaktionssicher) mit der Festplatte synchronisiert. Selten verwendete Dateien werden aus dem RAM entfernt -- benötigte, aber nicht vorhandene Dateien in den RAM gelesen.

</details>

## Die "natürliche" Form von Programmcode

**Definition:** Ein Datenformat ist "natürlich", wenn es über alle vorgesehenen Anwendungsfälle hinweg insgesamt die besten Eigenschaften besitzt. Außerdem gibt es keinen Anwendungsfall, für den ein anderes Datenformat deutlich bessere Eigenschaften besitzen würde.

Es wurden verschiedene mögliche Formate für Programmcode untersucht und verglichen. Die besten Eigenschaften wurden mit einer Kombination aus Text-Format und Graph-Format erzielt:

* **Text-Repräsentation:** Die bekannteste Form von Programmcode ist seine Text-Form. Diese kann von Menschen gelesen und bearbeitet werden. Auch viele Programmierwerkzeuge arbeiten auf dieser Ebene. Die primäre Struktur ist daher das Text-Format.

* **Graph-Repräsentation:** Die innere Struktur von Programmcode ist sicherlich ein Graph. Nur in dieser Form kann der Computer den Programmcode verarbeiten. Die Kombination von Text- und Graph-Struktur ist ein Graph, der die originalen Text-Fragmente enthält.

     Bei Änderung des Texts wird der entsprechende Teil des Graphs zunächst deaktiviert und später mit dem neu kompilierten Teilgraph zusammengeführt. Nicht zusammenführbare Informationen werden nicht verworfen, aber bleiben inaktiv.

     Bei Änderung des Graphs wird der entsprechende Text aus dem Graph "rückwärts kompiliert". Zuvor enthaltene Formatierungen werden mit dem neu erzeugten Text zusammengeführt.
* **Unsichtbare Markierungen:** Für die Darstellung im Editor können sogenannte "unsichtbare Markierungen" in den Text eingefügt werden. Mit diesen können die Darstellung formatiert oder grafische Elemente ausgerichtet werden.

## Das Datenformat II

...

## Die Abfragesprache

...

* Grundmenge
* Prädikate
* Operatoren
* Graph-Suche

Schnittstelle

## Definition der Schnittstellen

...

## Die statische Analyse

...

## Installation

0. Preliminary
     1. unpack [gcc-12.2.0](https://ftp.gnu.org/gnu/gcc/gcc-12.2.0/) to ~
     2. run ~/gcc-12.2.0$ /contrib/download_prerequisites
     3. run ~/gcc-12.2.0$ ./configure
     4. create ~/demo.c
     5. copy store_graph_to_disk.cc to ~/gcc-12.2.0/gcc and apply toplev.patch

1. ~/gcc-12.2.0$ make -j 64
2. ~$ STORE_GRAPH_TO_DISK= ./gcc-12.2.0/host-x86_64-pc-linux-gnu/stage1-gcc/cc1 demo.c 3>&1
